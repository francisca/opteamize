# -*- encoding:utf-8 -*-
# import json

from django.conf.urls import url
from django.contrib.auth import authenticate, login

from tastypie.resources import ModelResource
# from tastypie import fields
from tastypie.models import ApiKey
from tastypie.authorization import Authorization
from tastypie.utils import trailing_slash
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)

from api.utils import urlencodeSerializer
from api.exception import CustomBadRequest
from tps.settings.base import *

from users.models import User

class AuthApi(ModelResource):

    class Meta:
        queryset = User.objects.all()
        allowed_methods = ['post', 'put']
        authorization = Authorization()
        serializer = urlencodeSerializer()
        resource_name = 'auth'
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r"^(?P<resource_name>%s)/register%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('register'), name="api_login"),

        ]

    def login(self, request, **kwargs):

        self.method_check(request, allowed=['post', ])
        data = self.deserialize(request, request.body)
        rut = data.get('rut', '')
        password = data.get('password', '')
        user = authenticate(rut=rut, password=password)

        if user:
            token, created = ApiKey.objects.get_or_create(user=user)
            if user.is_active:
                login(request, user)
                if user.avatar:
                    avatar = "/media/%s" % user.avatar
                else:
                    avatar = "/media/ima-default-profile-2.png"

                return self.create_response(request, {
                    'rut': user.rut,
                    'resource_uri': '/api/v1/user/%s/' % user.id,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'email': user.email,
                    'id': user.id,
                    'token': token.key,
                    'avatar': avatar,
                    'contact_phone': user.contact_phone
                })

        else:
            raise CustomBadRequest(
                code=701,
                message="Datos incorrectos. Intente nuevamente")

    def register(self, request, **kwargs):
        self.method_check(request, allowed=['post', ])
        data = self.deserialize(request, request.body)
        
        rut = data.get('rut')
        email = data.get('email')
        first_name = data.get('first_name')
        last_name = data.get('last_name')
        password = data.get('password')
        contact_phone = data.get('contact_phone')

        # Validacion email & rut
        if User.objects.filter(email=data.get('email')).count() > 0:
            message = u"Este e-mail está en uso. Intente con uno diferente o pruebe a recuperar su contraseña."
            raise CustomBadRequest(code=700,
                    message=message)

        if User.objects.filter(rut=data.get('rut')).count() > 0:
            message = u"Este RUT está en uso. Intente con uno diferente o pruebe a recuperar su contraseña."
            raise CustomBadRequest(code=700,
                    message=message)

        try:
            """
            # Registramos un nuevo usuario
            """
            user = User.objects.create(
                rut=rut,
                email=email,
                first_name=first_name,
                last_name=last_name,
                contact_phone=contact_phone
            )
            user.set_password(password)
            user.save()

            token, created = ApiKey.objects.get_or_create(user=user)

            response_data = {
                'rut': user.rut ,
                'resource_uri': '/api/v1/user/%s/' % user.id,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'id': user.id,
                'token': token.key,
                'contact_phone': contact_phone,
                'avatar': "/media/ima-default-profile-1.png"
            }

            return self.create_response(request, response_data)

        except Exception as e:
            print e
            raise CustomBadRequest(
                code=701,
                message="Error al registrar usuario. Intente nuevamente.")
