# -*- encoding: utf-8 -*-
from tastypie import fields
from tastypie.resources import ModelResource, ALL
from tastypie.authorization import Authorization
from datetime import datetime
from tastypie.authentication import (
	MultiAuthentication,
	ApiKeyAuthentication,
	SessionAuthentication
	)
from weather.models import Weather
import requests

class WeatherApi(ModelResource):

	class Meta:
		queryset = Weather.objects.all()
		resource_name = "weather"
		authorization = Authorization()
		always_return_data = True
		allowed_methods = ['get', 'post', 'put', 'delete']
		authentication = MultiAuthentication(ApiKeyAuthentication(), SessionAuthentication())

		filtering = {
			"date": ALL
		}


	def alter_list_data_to_serialize(self, request, data):
		if data["meta"]["total_count"] == 0:
			yahoo_request = requests.get("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20valparaiso,cl%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
			yahoo_response = yahoo_request.json()
			for key, value in yahoo_response.items():
				for key_query, value_query in value.items():
					if key_query == "results":
						for key_results, value_results in value_query.items():
							yahoo_weather = value_results["item"]["forecast"][0]
							max_temp = (int(yahoo_weather["high"])-32) * 0.56
							min_temp = (int(yahoo_weather["low"])-32) * 0.56
							weather_code = yahoo_weather["code"]
							weather_desc = yahoo_weather["text"]
							date = datetime.now().date()

			daily_weather = {}
			new_weather = Weather.objects.create(date=date, max_temp=int(round(max_temp)), min_temp=int(round(min_temp)), weather_code=weather_code, weather_desc=weather_desc)
			daily_weather["date"] = new_weather.date
			daily_weather["max_temp"] = new_weather.max_temp
			daily_weather["min_temp"] = new_weather.min_temp
			daily_weather["weather_code"] = new_weather.weather_code
			daily_weather["weather_desc"] = new_weather.weather_desc
			daily_weather["resource_uri"] = "/api/v1/weather/%s/" % new_weather.id
			daily_weather["id"] = new_weather.id
			data["objects"].append(daily_weather)
			return data
		else:
			return data

			# r = requests.get("http://api.worldweatheronline.com/premium/v1/weather.ashx?key=9fde65b5db3948a2b5b152935170706&q=-33.0339143,-71.6318113&num_of_days=1&tp=1&lang=es&format=json")
			# response = r.json()
			# weather = response["data"]["weather"]

			# for current in response["data"]["current_condition"]:
			# 	weather_code = current["weatherCode"]
			# 	for weatherDesc in current["lang_es"]:
			# 		weather_desc = weatherDesc["value"]
			
			# for wt in weather:
			# 	date = wt["date"]
			# 	max_temp = wt["maxtempC"]
			# 	min_temp = wt["mintempC"]



