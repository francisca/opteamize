# -*- encoding: utf-8 -*-
from django.conf.urls import url
from tastypie.utils import trailing_slash
from tastypie import fields
from tastypie.resources import ModelResource, ALL
from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication
from users.models import User
from users.forms import PasswordResetForm
from api.exception import CustomBadRequest


class UserPasswordApi(ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'user_password'
        always_return_data = True
        authorization = Authorization()
        authentication = ApiKeyAuthentication()
        allowed_methods = ["get", "put", "post"]
        ordering = ['first_name', 'last_name', 'email']
        excludes = ('password', 'is_active',
            'is_superuser', 'is_staff')
        filtering = {
            'is_admin': ALL
        }


    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/change%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('change_password'), name="api_passwordreset_change"),
            
            url(r"^(?P<resource_name>%s)/reset%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('reset'), name="api_passwordreset_mail_token"),
      
        ]


    def change_password(self, request, **kwargs):
        self.is_authenticated(request)
        self.method_check(request, allowed=["put"])
        data = self.deserialize(request, request.body)
        old_password = data.get("old_password")
        new_password = data.get("new_password")
        ## Validamos si la password actual corresponde
        
        user = request.user
        print user
        valid_password = user.check_password(old_password)

        if not new_password:
            raise CustomBadRequest(code=702,
                message="Debe ingresar una nueva password")

        if valid_password:
            user.set_password(new_password)
            user.save()
            return self.create_response(request, {"message": "Su password fue actualizada existosamente" })
        else:
            raise CustomBadRequest(code=702,
                message="La password actual no coincide con la ingresada")


    def reset(self, request, **kwargs):
        self.method_check(request, allowed=['post'])
        self.throttle_check(request)

        jsonData = self.deserialize(request, request.body)
        email = jsonData.get("email")
        form = PasswordResetForm({'email': email})
        if form.is_valid():
            form.save()
            return self.create_response(request, {'message': 'Pronto recibirás un correo con las instrucciones para restablecer tu contraseña'})
        else:
            raise CustomBadRequest(code=702,
                message="Error, intente nuevamente")