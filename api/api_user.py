# -*- encoding:utf-8 -*-
from tastypie.resources import ModelResource, ALL
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)
from api.utils import ModelFormValidation
from users.forms import UserForm
from users.models import User
from django.contrib.auth.models import Group
from api.exception import CustomBadRequest
from django.db.models import Q


class UserApi(ModelResource):
    group = fields.ForeignKey('api.api_user.GroupApi', 'group', full=True, null=True)

    class Meta:
        queryset = User.objects.all()
        allowed_methods = ['get', 'put', 'post', 'patch']
        resource_name = 'user'
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        always_return_data = True
        validation = ModelFormValidation(form_class=UserForm)
        fields = (
            'rut','first_name', 'last_name',
            'id', 'email', 'is_superuser',
            'contact_phone')
        filtering = {
            "is_superuser": ALL,
            "rut": ALL,
            "first_name": ALL,
            "last_name": ALL,
            "query": ['icontains',],
        }

    def obj_create(self, bundle, **kwargs):
        bundle = super(UserApi, self).obj_create(bundle, **kwargs)
        bundle.obj.set_password(bundle.data["password"])
        bundle.obj.save()
        return bundle

    def build_filters(self, filters=None, ignore_bad_filters=False):
        if filters is None:
            filters = {}
        orm_filters = super(UserApi, self).build_filters(filters)

        if('query' in filters):
            query = filters['query']
            qset = (
                    Q(first_name__icontains=query) |
                    Q(last_name__icontains=query)
                    )
            orm_filters.update({'custom': qset})

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        if 'custom' in applicable_filters:
            custom = applicable_filters.pop('custom')
        else:
            custom = None

        semi_filtered = super(UserApi, self).apply_filters(request, applicable_filters)

        return semi_filtered.filter(custom) if custom else semi_filtered

class AdminUpdateApi(ModelResource):
    group = fields.ForeignKey('api.api_user.GroupApi', 'group', full=True, null=True)
    
    class Meta:
        queryset = User.objects.all()
        allowed_methods = ['get', 'post', 'put', 'patch']
        resource_name = 'user_update'
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        always_return_data = True
        fields = ('first_name', 'last_name', 'id', 'email', 'contact_phone', 'group')

        filtering = {
            "is_superuser": ALL
        }
    
    def hydrate(self, bundle):
    # Validacion email

        print bundle

        if User.objects.filter(email=bundle.data.get('email')).count() > 0:
            if User.objects.filter(email=bundle.data.get('email'), id=bundle.data.get('pk')).count() == 1:
                return bundle
            else:
                message = u"Este email está en uso. Intente con uno diferente para actualizar sus datos."
                raise CustomBadRequest(code=700,
                        message=message)
        else:
            return bundle

    def alter_detail_data_to_serialize(self, request, data):
        data.data["resource_uri"] = "/api/v1/user/%s/" % data.data["id"]
        return data


class AdminUpdateCMSApi(ModelResource):
    class Meta:
        queryset = User.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        resource_name = 'admin_update'
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        always_return_data = True
        
    def obj_update(self, bundle, **kwargs):
        super(AdminUpdateCMSApi, self).obj_update(bundle, **kwargs)
        bundle.obj = self.obj_get(bundle=bundle, **kwargs)

        user = User.objects.get(id=bundle.data["id"])

        try: 
            user.set_password(bundle.data["password"])
            user.save()
        except: 
            print "No change password"
        
        return bundle



class GroupApi(ModelResource):

    class Meta:
        queryset = Group.objects.all()
        allowed_methods = ['get', 'post', 'put', 'delete']
        resource_name = 'group'
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        always_return_data = True


