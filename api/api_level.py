# -*- encoding:utf-8 -*-
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db.models.functions import Lower
from django.db.models import Count
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)
from api.utils import (ModelFormValidation)
from level.models import (
    Level,
    Evaluation
    )

class LevelApi(ModelResource):

    class Meta:
        queryset = Level.objects.all()
        allowed_methods = ['get','post','put','delete']
        resource_name = 'level'
        authorization = Authorization()
        # authentication = MultiAuthentication(
        #     ApiKeyAuthentication(),
        #     SessionAuthentication())
        always_return_data = True


class EvaluationApi(ModelResource):

    class Meta:
        queryset = Evaluation.objects.all()
        allowed_methods = ['get','post','put','delete']
        resource_name = 'evaluation'
        authorization = Authorization()
        #authentication = MultiAuthentication(
        #    ApiKeyAuthentication(),
        #    SessionAuthentication())
        always_return_data = True