# -*- encoding: utf-8 -*-
from tastypie import fields
from tastypie.resources import ModelResource
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    SessionAuthentication,
    ApiKeyAuthentication)

from users.models import User
from api.utils import MultipartResource
from api.exception import CustomBadRequest


class AvatarApi(MultipartResource, ModelResource):

    class Meta:
        queryset = User.objects.all()
        resource_name = 'avatar'
        allowed_method = ['put', ]
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        fields = ['avatar']
        always_return_data = True

    def obj_update(self, bundle, **kwargs):
        bundle.obj = self.obj_get(bundle=bundle, **kwargs)

        if bundle.obj.pk != bundle.request.user.pk:
            raise CustomBadRequest(code=906,
                    message="No tiene los permisos necesarios")
        
        return super(AvatarApi, self).obj_update(
            bundle, **kwargs)
