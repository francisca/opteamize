# -*- encoding: utf-8 -*-
from tastypie.resources import ModelResource
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import ApiKeyAuthentication, SessionAuthentication, MultiAuthentication
from tastypie.validation import Validation

from push_notifications.models import APNSDevice, GCMDevice
from users.models import User

from api.api_user import UserApi
from api.utils import urlencodeSerializer
from tastypie.resources import ALL_WITH_RELATIONS


class ApnsRegistrationIdValidation(Validation):

    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': 'No data provided'}

        errors = {}
        if APNSDevice.objects.filter(registration_id=bundle.data["registration_id"]).exists():
            errors["registration_id"] = "El token ya se encuentra registrado"

        return errors


class TokenApnsApi(ModelResource):
    user = fields.ForeignKey(UserApi, 'user', full=True, null=True)

    class Meta:
        queryset = APNSDevice.objects.order_by('-id')
        allowed_method = ['get', 'post', 'put', 'delete']
        resource_name = 'device_apns'
        filtering = {
            'user': ALL_WITH_RELATIONS
        }
        authorization = Authorization()
        authentication = MultiAuthentication(SessionAuthentication(),
            ApiKeyAuthentication())
        always_return_data = True
        #validation = ApnsRegistrationIdValidation()


    def delete_detail(self, request, **kwargs):
        try:
            super(TokenApnsApi, self).delete_detail(request, **kwargs)
        except:
            pass
        return self.create_response(request, {
                    'status': 'ok',
                    'message': 'El token ha sido eliminado exitosamente'
                    })
  
    def alter_detail_data_to_serialize(self, request, data):
        new_data = {}
        new_data["status"] = "ok"
        
        token_detail = {}
        token_detail["id"] = data.data["id"]
        token_detail["device_id"] = data.data["device_id"]
        token_detail["name"] = data.data["name"]
        token_detail["registration_id"] = data.data["registration_id"]
        token_detail["resource_uri"] = data.data["resource_uri"]
        user = data.data["user"]
        token_detail["user"] = user.data["id"]
        
        new_data["results"] = token_detail

        return new_data

    def obj_create(self, bundle, **kwargs):
        device = APNSDevice.objects.filter(registration_id=bundle.data["registration_id"])
        if device.exists():
            device_user = APNSDevice.objects.get(id=device[0].id)
            user_split = bundle.data["user"].split("/")
            user = User.objects.get(id=user_split[4])
            device_user.name = bundle.data["name"]
            device_user.user = user
            print device_user.user
            device_user.save()
            apns_device = device_user
            apns_bundle = self.build_bundle(request=None, obj=apns_device)            
            return apns_bundle

        return super(TokenApnsApi, self).obj_create(bundle, **kwargs)


class TokenGCMDApi(ModelResource):
    user = fields.ForeignKey(UserApi, 'user', full=True, null=True)
    
    class Meta:
        queryset = GCMDevice.objects.order_by('-id')
        allowed_method = ['get', 'post', 'put', 'delete']
        resource_name = 'device_gcm'
        filtering = {
            'user': ALL_WITH_RELATIONS
        }
        authorization = Authorization()
        serializer = urlencodeSerializer()
        authentication = MultiAuthentication(SessionAuthentication(),
            ApiKeyAuthentication())
        always_return_data = True
        #validation = GcmRegistrationIdValidation()

    def delete_detail(self, request, **kwargs):
        try:
            super(TokenGCMDApi, self).delete_detail(request, **kwargs)
            return self.create_response(request, {
                    'status':'ok',
                    'message': 'El token ha sido eliminado exitosamente'
                    })      
        except:
            pass

    def alter_detail_data_to_serialize(self, request, data):
        new_data = {}
        new_data["status"] = "ok"
        token_data = []
        token_detail = {}
        token_detail["id"] = data.data["id"]
        token_detail["device_id"] = data.data["device_id"]
        token_detail["name"] = data.data["name"]
        token_detail["registration_id"] = data.data["registration_id"]
        token_detail["resource_uri"] = data.data["resource_uri"]
        user = data.data["user"]
        token_detail["user"] = user.data["id"]
        new_data["results"] = token_detail
        
        return new_data     

    def obj_create(self, bundle, **kwargs):
        device = GCMDevice.objects.filter(registration_id=bundle.data["registration_id"])
        if device.exists():
            device_user = GCMDevice.objects.get(id=device[0].id)
            user_split = bundle.data["user"].split("/")
            user = User.objects.get(id=user_split[4])
            device_user.name = bundle.data["name"]
            device_user.user = user
            print device_user.user
            device_user.save()
            gcm_device = device_user
            gcm_bundle = self.build_bundle(request=None, obj=gcm_device)            
            return gcm_bundle

        return super(TokenGCMDApi, self).obj_create(bundle, **kwargs)
