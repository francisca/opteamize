# -*- encoding:utf-8 -*-
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db.models.functions import Lower
from django.db.models import Count
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)
from api.utils import (ModelFormValidation)
from contact.models import (
    Contact,
    ContactUser
    )
from contact.forms import ContactForm


class ContactApi(ModelResource):

    class Meta:
        queryset = Contact.objects.filter(visible=1).order_by(Lower('name'))
        allowed_methods = ['get','post','put','patch']
        resource_name = 'contact'
        authorization = Authorization()
        always_return_data = True

        filtering = {
            "contact_type": ALL
        }

        # travels = Travel.objects.filter(finish_type=0)
        # last_ids = []
        # for travel in travels:
        #     user_location = UserLocation.objects.filter(travel=travel).last()
        #     if user_location != None:
        #         last_ids.append(user_location.id)
        # user_location_order = super(UserLocationApi, self).get_object_list(request).filter(pk__in=last_ids).order_by('travel_step', 'eta')
        # return user_location_order


class ContactUserApi(ModelResource):
    contact = fields.ForeignKey(ContactApi, 'contact', full=True)
    user = fields.ForeignKey('api.api_user.UserApi', 'user', full=False)

    class Meta:
        queryset = ContactUser.objects.all()
        allowed_methods = ['get','post']
        resource_name = 'contact_user'
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        always_return_data = True

        filtering = {
            "user": ALL
        }



