# -*- encoding:utf-8 -*-
from tastypie.resources import ModelResource, ALL
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)
from container.models import ContainerAlgorithm


class ContainerAlgorithmApi(ModelResource):

    class Meta:
        queryset = ContainerAlgorithm.objects.all()
        allowed_methods = ['get','post']
        resource_name = 'container_algorithm'
        authorization = Authorization()
        authentication = MultiAuthentication(
            ApiKeyAuthentication(),
            SessionAuthentication())
        always_return_data = True