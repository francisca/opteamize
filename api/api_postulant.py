# -*- encoding:utf-8 -*-
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db.models.functions import Lower
from django.db.models import Count
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)
from api.utils import (ModelFormValidation)
from skill.models import (
    Skill
    )

from postulant.models import (
    Postulant,
    PostulantCertification,
    PostulantSkillSoft,
    PostulantExperience
    )


class PostulantCertificationApi(ModelResource):
    postulant = fields.ForeignKey('api.api_postulant.PostulantApi', 'postulant')

    class Meta:
        queryset = PostulantCertification.objects.all()
        allowed_methods = ['get','post', 'put', 'delete']
        resource_name = 'postulant_certification'
        authorization = Authorization()
        # authentication = MultiAuthentication(
        #     ApiKeyAuthentication(),
        #     SessionAuthentication())
        always_return_data = True

        filtering = {
            "postulant"
        }


class PostulantApi(ModelResource):
    evaluation = fields.ForeignKey('api.api_level.EvaluationApi', 'evaluation', full=True)
    level = fields.ForeignKey('api.api_level.LevelApi', 'level', full=True)
    skills = fields.ToManyField('api.api_skill.SkillApi',
        'skills', full=True)
    postulant_certifications = fields.ToManyField(PostulantCertificationApi, attribute=lambda bundle: PostulantCertification.objects.filter(postulant=bundle.obj), related_name="postulant_certifications", full=True, null=True)

    class Meta:
        queryset = Postulant.objects.all()
        allowed_methods = ['get','post', 'put', 'delete']
        resource_name = 'postulant'
        authorization = Authorization()
        # authentication = MultiAuthentication(
        #     ApiKeyAuthentication(),
        #     SessionAuthentication())
        always_return_data = True


class PostulantSkillSoftApi(ModelResource):
    postulant = fields.ForeignKey('api.api_postulant.PostulantApi', 'postulant')

    class Meta:
        queryset = PostulantSkillSoft.objects.all()
        allowed_methods = ['get','post', 'put', 'delete']
        resource_name = 'postulant_skill_soft'
        authorization = Authorization()
        # authentication = MultiAuthentication(
        #     ApiKeyAuthentication(),
        #     SessionAuthentication())
        always_return_data = True

        filtering = {
            "postulant"
        }


class PostulantExperienceApi(ModelResource):
    postulant = fields.ForeignKey('api.api_postulant.PostulantApi', 'postulant')

    class Meta:
        queryset = PostulantExperience.objects.all()
        allowed_methods = ['get','post', 'put', 'delete']
        resource_name = 'postulant_experience'
        authorization = Authorization()
        # authentication = MultiAuthentication(
        #     ApiKeyAuthentication(),
        #     SessionAuthentication())
        always_return_data = True

        filtering = {
            "postulant"
        }