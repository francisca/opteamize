# -*- encoding: utf-8 -*-
from users.models import User
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

class AdminMixinAccess(object):

	def dispatch(self, request, *args, **kwargs):
		
		return super(AdminMixinAccess, self).dispatch(request, *args, **kwargs)