# -*- encoding: utf-8 -*-
from django.conf.urls import url, include
from tastypie.api import Api

# Urls api
v1_api = Api(api_name='v1')

# importamos clases API

from api.auth import AuthApi
v1_api.register(AuthApi())

# API - Usuarios
from api.api_user import (
    UserApi,
    AdminUpdateApi,
    AdminUpdateCMSApi,
    GroupApi)

v1_api.register(UserApi())
v1_api.register(AdminUpdateApi())
v1_api.register(AdminUpdateCMSApi())
v1_api.register(GroupApi())

# API . Avatar
from api.api_avatar import AvatarApi
v1_api.register(AvatarApi())

# API - Password Usuarios
from api.api_user_password import (
    UserPasswordApi)
v1_api.register(UserPasswordApi())


# API - Contact
from api.api_contact import (
    ContactApi,
    ContactUserApi,
    )

v1_api.register(ContactApi())
v1_api.register(ContactUserApi())

# API - Postulant
from api.api_postulant import (
	PostulantApi,
	PostulantSkillSoftApi,
	PostulantCertificationApi,
	PostulantExperienceApi
	)
v1_api.register(PostulantApi())
v1_api.register(PostulantSkillSoftApi())
v1_api.register(PostulantCertificationApi())
v1_api.register(PostulantExperienceApi())


# API - Skill
from api.api_skill import (
	SkillApi,
	SkillSoftApi
	)
v1_api.register(SkillApi())
v1_api.register(SkillSoftApi())


# API - Level
from api.api_level import (
	LevelApi,
	EvaluationApi)
v1_api.register(LevelApi())
v1_api.register(EvaluationApi())


# API - University
# from api.api_university import (
# 	UniversityApi)
# v1_api.register(UniversityApi())


# API - Weather
from api.api_weather import WeatherApi
v1_api.register(WeatherApi())


urlpatterns = [
    url(r'^', include(v1_api.urls)),
]