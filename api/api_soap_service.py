# -*- encoding: utf-8 -*-
# Control de servicios WS turbus
import json
from pysimplesoap.client import SoapClient

class TPSContainerStatus:

    def __init__(self):
        self.user = "TPSV"
        self.password = "test"
        self.tps_wsdl = "http://prma.tpsv.cl:8181/SmartTruck/SmartTruck.asmx?WSDL"


    def get_route_status(self, date, containerDate):
        
        client = SoapClient(wsdl=self.tps_wsdl, trace=False)
        req = client.ConsEstadosRuta(
            dataIn=date,
            User=self.user,
            Password=self.password)

        data = req["ConsEstadosRutaResult"].split('<')

        row_to_return = {}
        data_to_return = {}

        for row in data:
            if not row.startswith("/"):
                if "respuestaEstadoRuta" not in row:
                    if ">" in row:
                        if len(row.split('>'))>0:
                            if "estadoSistema" in row.split('>')[0]:
                                row_to_return["semaphore"] = row.split('>')[1]
                                data_to_return["routeStatus"] = row_to_return
                            else:
                                row_to_return[row.split('>')[0]] = int(row.split('>')[1])
                                data_to_return["routeStatus"] = row_to_return
                        else:
                            row_to_return[row.replace('>','')] = ""
                            data_to_return["routeStatus"] = row_to_return

        # <respuestaEstadoRuta>
        # <T0>
        # <T1>
        # <T2>
        # <T3>
        # <estadoSistema>

        return data_to_return

    
    def get_procedure_status(self, container, containerNum):
        
        client = SoapClient(wsdl=self.tps_wsdl, trace=False)
        req = client.ConsEstadoTramiteContenedor(
            dataIn=container,
            User=self.user,
            Password=self.password)

        data = req["ConsEstadoTramiteContenedorResult"].split('<')

        row_to_return = {}
        data_to_return = {}

        for row in data:
            if not row.startswith("/"):
                if "respuestaEstadoContenedor" not in row:
                    if ">" in row:
                        if len(row.split('>'))>0:
                            row_to_return[row.split('>')[0]] = row.split('>')[1]
                            data_to_return[containerNum] = row_to_return
                        else:
                            row_to_return[row.replace('>','')] = ""
                            data_to_return[containerNum] = row_to_return
                
        # <respuestaEstadoContenedor>
        # <estadoContenedor>
        # <fechaInicoStacking>
        # <fechaCierreStacking>
        # <posicionContenedor>
        # <unitReceiveTps>

        return data_to_return