## -*- encoding:utf-8 -*-
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from django.db.models.functions import Lower
from django.db.models import Count
from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.authentication import (
    MultiAuthentication,
    ApiKeyAuthentication,
    SessionAuthentication)
from api.utils import (ModelFormValidation)
from skill.models import (
    Skill,
    SkillSoft
    )

class SkillApi(ModelResource):

    class Meta:
        queryset = Skill.objects.all()
        allowed_methods = ['get','post']
        resource_name = 'skill'
        authorization = Authorization()
        #authentication = MultiAuthentication(
        #    ApiKeyAuthentication(),
        #    SessionAuthentication())
        always_return_data = True


class SkillSoftApi(ModelResource):

    class Meta:
        queryset = SkillSoft.objects.all()
        allowed_methods = ['get','post']
        resource_name = 'skill_soft'
        authorization = Authorization()
        #authentication = MultiAuthentication(
        #    ApiKeyAuthentication(),
        #    SessionAuthentication())
        always_return_data = True