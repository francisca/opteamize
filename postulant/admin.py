from django.contrib import admin
from postulant.models import (
	Postulant,
	PostulantCertification,
	PostulantExperience
	)

admin.site.register(Postulant)
admin.site.register(PostulantCertification)
admin.site.register(PostulantExperience)
