# -*- encoding: utf-8 -*-
from django.conf.urls import url

from postulant.views import (
	PostulantListView,
	PostulantCreateView
	)

urlpatterns = [
	url(r'^$', PostulantListView.as_view(), name='postulant_list'),
	url(r'^add/$', PostulantCreateView.as_view(), name='postulant_add')
 #   	url(r'^(?P<pk>\d+)/update/$', ContactUpdateView.as_view(), name='contact_update'),
]