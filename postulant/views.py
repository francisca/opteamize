from django.shortcuts import render

from django.views.generic import (
	TemplateView,
	FormView)

from postulant.forms import PostulantForm

from braces.views import LoginRequiredMixin

class PostulantListView(LoginRequiredMixin, TemplateView):
	template_name = 'postulant/index.html'
	login_url = '/'

class PostulantCreateView(LoginRequiredMixin, FormView):
	template_name = 'postulant/add.html'
	login_url = '/'
	form_class = PostulantForm