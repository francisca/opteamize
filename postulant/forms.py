# -*- encoding: utf-8 -*-
from django import forms
from django.core.validators import URLValidator, RegexValidator

from crispy_forms.helper import FormHelper

from postulant.models import (
	Postulant
	)


class PostulantForm(forms.ModelForm):

	class Meta:
		model = Postulant
		fields = ('name', 'rut', 'birth_date', 'email', 'evaluation', 'university', 'skills', 'level', 'interview_date')

	def __init__(self, *args, **kwargs):
		super(PostulantForm, self).__init__(*args, **kwargs)
		self.fields["name"].label = "Postulant Full Name"
		self.fields["rut"].label = "RUT"
		self.fields['birth_date'].label = "Birth Date"
		self.fields['email'].label = "Email"
		self.fields['evaluation'].label = "Evaluation"
		self.fields['university'].label = "University"
		self.fields['skills'].label = "Technical Skills"
		self.fields['level'].label = "Level for which she/he postulates"
		self.fields['interview_date'].label = "Interview Date"

		self.helper = FormHelper()
		self.helper.form_id = 'postulant_form'
