# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.db import models
from university.models import University
from skill.models import Skill, SkillSoft
from level.models import Level, Evaluation

class Postulant(models.Model):
	name = models.CharField(max_length=200)
	rut = models.CharField(
        max_length=15,
        unique=True,
        db_index=True)
	birth_date = models.DateField(null=True)
	email = models.EmailField(
        max_length=255,
        db_index=True,
        null=True)
	visible = models.IntegerField(default=1)
	evaluation = models.ForeignKey(Evaluation, null=True)
	university = models.ForeignKey(University, null=True)
	skills = models.ManyToManyField(Skill)
	level = models.ForeignKey(Level, null=True)
	interview_date = models.DateField(null=True)
	income_claim = models.IntegerField(null=True)

	def __unicode__(self):
		return "%s" % self.name

	class Meta:
		app_label = 'postulant'
		db_table = 'postulant'


class PostulantSkillSoft(models.Model):
	postulant = models.ForeignKey(Postulant, null=True)
	skill_soft = models.ForeignKey(SkillSoft, null=True)
	score = models.IntegerField(default=0)

	def __unicode__(self):
		return "%s" % self.postulant.name

	class Meta:
		app_label = 'postulant'
		db_table = 'postulant_skill_soft'


class PostulantCertification(models.Model):
	postulant = models.ForeignKey(Postulant, null=True)
	certification_name = models.CharField(max_length=200)
	year = models.DateField(null=True)
	institution = models.CharField(max_length=200)

	def __unicode__(self):
		return "%s" % self.postulant.name

	class Meta:
		app_label = 'postulant'
		db_table = 'postulant_certification'


class PostulantExperience(models.Model):
	postulant = models.ForeignKey(Postulant, null=True)
	year_from = models.DateField(null=True)
	year_to = models.DateField(null=True)
	work_position = models.CharField(max_length=200)
	organitation = models.DateField(max_length=200, null=True)

	def __unicode__(self):
		return "%s" % self.postulant.name

	class Meta:
		app_label = 'postulant'
		db_table = 'postulant_experience'