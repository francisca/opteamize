from django.contrib import admin
from skill.models import (
	Skill,
	SkillSoft
	)

admin.site.register(Skill)
admin.site.register(SkillSoft)
