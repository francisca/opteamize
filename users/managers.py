# -*- encoding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager)


class UserManager(BaseUserManager):

    def create_user(self, rut, password=None):

        if not rut:
            msg = "User must have a rut"
            print msg

        user = self.model(
            rut=rut)

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, rut, password):
        user = self.create_user(rut, password=password)
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)

        return user
