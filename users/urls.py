# -*- encoding: utf-8 -*-
from django.conf.urls import url
from users.views import (
	UserListView,
	UserUpdateView,
	AdminListView,
	AdminUpdateView,
	AdminCreateView,
	AdminUpdatePasswordView
	)

urlpatterns = [
	url(r'^$', UserListView.as_view(), name='user_list'),
	url(r'^exportar/', 'users.export.export_users', name='user_export'),
	url(r'^(?P<pk>\d+)/$', UserUpdateView.as_view(), name='user_update'),
	url(r'^administrator/$', AdminListView.as_view(), name='admin_list'),
	url(r'^administrator/add/$', AdminCreateView.as_view(), name='admin_add'),
	url(r'^administrator/(?P<pk>\d+)/update/$', AdminUpdateView.as_view(), 
     	name='admin_update'),
	url(r'^administrator/password/(?P<pk>\d+)/$', AdminUpdatePasswordView.as_view(),
	    name='admin_update_password'),
]

