# -*- encoding: utf-8 -*-
from django.http import HttpResponse
from django.core import serializers
from datetime import datetime

from users.models import User

import json
import xlsxwriter
try:
	import cStringIO as StringIO
except ImportError:
	import StringIO

def format_dateexcel(date_format):
	date_format = date_format.strftime("%Y-%m-%d %H:%M:%S")
	new_date = date_format.split(" ")
	day = new_date[0]
	new_day = day.split("-")
	hour = new_date[1]
	return "%s-%s-%s %s" % (new_day[2], new_day[1], new_day[0], hour)


def export_users(request):
	output = StringIO.StringIO()
	workbook = xlsxwriter.Workbook(output)
	worksheet = workbook.add_worksheet('Usuarios')

	header_keys = []

	file = 0
	column = 0

	format2 = workbook.add_format({'bold': True, 'font_color': 'white', 'bg_color': '#4e3c6e'})
	
	# worksheet.write(0, 0, u"TRIVIA", format2)
	# worksheet.write(1, 0, u"FECHA INICIO", format2)
	# worksheet.write(2, 0, u"FECHA TÉRMINO", format2)

	#questionary = Report.objects.get(id=pk)

	# worksheet.write(0, 1, u"%s" % questionary.title)
	# worksheet.write(1, 1, u"%s" % format_dateexcel(questionary.date_init))
	# worksheet.write(2, 1, u"%s" % format_dateexcel(questionary.date_finish))


	format = workbook.add_format({'bold': True, 'font_color': 'white',
		'bg_color': 'green'})

	worksheet.write(1, 0, u"NOMBRE", format)
	worksheet.write(1, 1, u"EMAIL", format)

	file = 2
	column = 0

	for user in User.objects.filter(is_superuser=False):
		worksheet.write(file, column, u"%s, %s" % (user.last_name, user.first_name))
		column += 1

		worksheet.write(file, column, u"%s" % (user.email))
		column += 1

		# result_answer = answer.answer
		# answer_json = json.loads(result_answer)
		# worksheet.write(file, column, u"%s" % answer_json["name"])
		# column += 1

		# worksheet.write(file, column, u"%s" % format_dateexcel(answer.date))
		# column += 1

		# worksheet.write(file, column, u"%s" % answer.attemps)
		# column += 1
		
		column = 0
		file += 1
	
	workbook.close()
	output.seek(0)

	today = datetime.now().date()


	filename = "TPS-GO-Usuarios"+str(today)+".xlsx"
	response = HttpResponse(output.read(),
		content_type="application/ms-excel")
	response['Content-Disposition'] = 'attachement; filename=%s' % filename

	return response

