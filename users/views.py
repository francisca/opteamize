from django.shortcuts import render

from django.views.generic import (
	TemplateView,
	FormView)

from users.forms import UserForm, UserFormUpdate

from braces.views import LoginRequiredMixin

# Lista de usuarios registrados
class UserListView(LoginRequiredMixin, TemplateView):
	template_name = 'user/index.html'
	login_url = '/'

class UserUpdateView(LoginRequiredMixin, TemplateView):
	template_name = 'user/update.html'
	#form_class = UserForm
	login_url = '/'

	def get_context_data(self, *args, **kwargs):
		context = super(UserUpdateView, self).get_context_data(
			*args, **kwargs)
		context["user_id"] = self.kwargs["pk"]
		return context


#Lista administradores
class AdminListView(LoginRequiredMixin, TemplateView):
	template_name = 'administrator/index.html'
	login_url = '/'

class AdminCreateView(LoginRequiredMixin, FormView):
	template_name = 'administrator/add.html'
	form_class = UserForm
	login_url = '/'


class AdminUpdateView(LoginRequiredMixin, FormView):
	template_name = 'administrator/update.html'
	#form_class = UserForm
	login_url = '/'

	def get_context_data(self, *args, **kwargs):
		context = super(AdminUpdateView, self).get_context_data(
			*args, **kwargs)
		context["admin_id"] = self.kwargs["pk"]

		return context

	def get_form(self):
		print self.request
		return UserFormUpdate


class AdminUpdatePasswordView(TemplateView):
    template_name = 'administrator/password.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AdminUpdatePasswordView, self).get_context_data(*args, **kwargs)
        context["admin_id"] = self.kwargs["pk"]
        return context