# -*- encoding: utf-8 -*-
from __future__ import unicode_literals
import os
from django.db import models
from users.managers import UserManager
from django.contrib.auth.models import (
    AbstractBaseUser, 
    PermissionsMixin,
    Group
)

def user_image(instance, filename):
    return os.path.join("users/", filename)

class User(AbstractBaseUser, PermissionsMixin):
    rut = models.CharField(
        max_length=15,
        unique=True,
        db_index=True)
    email = models.EmailField(
        verbose_name='Email Address',
        max_length=255,
        db_index=True,
        null=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    contact_phone = models.CharField(max_length=12)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    status = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to=user_image, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    group = models.ForeignKey(Group, related_name='user_groups', null=True)


    objects = UserManager()

    USERNAME_FIELD = 'rut'

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return unicode(self.email) or u''

    class Meta:
        app_label = 'users'
        db_table = 'users'