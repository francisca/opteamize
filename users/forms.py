# -*- encoding: utf-8 -*-
from django import forms
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.tokens import default_token_generator
from users.models import User
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site


class AuthForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())


class PasswordResetForm(forms.Form):
    email = forms.EmailField(label="Email", max_length=254)
 
    # removed some parameters that I didn't use
    def save(self, subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             token_generator=default_token_generator,
             from_email=None, html_email_template_name='registration/password_reset_email.html'):
        from django.core.mail import send_mail
        # for some applications I use separate models for Django Admin/Web and API
        # get_api_user_model() returns my ApiUser, use django.contrib.auth.get_user_model()
        # for user model defined in settings.AUTH_USER_MODEL
        UserModel = get_user_model()

        email = self.cleaned_data['email']
        active_users = UserModel._default_manager.filter(
            email__iexact=email, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue
            # I use application name for both, in the email then I can point the user
            # to the application with an url like: application://passwordreset/secret_code
            # which on IOS and Android should open the app
            site_name = settings.APPLICATION_NAME
            site  = Site.objects.get(pk=settings.SITE_ID)
            protocol = 'https'
            c = {
                'email': user.email,
                'location': 'passwordreset',
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': protocol,
                'domain': site.domain
            }
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            email = loader.render_to_string(email_template_name, c)
 
            #if html_email_template_name:
            html_email = loader.render_to_string(html_email_template_name, c)
            plain_text_content = loader.render_to_string(email_template_name.replace('with_html', 'plaintext'), c)
            #else:
            #    html_email = None
            #send_mail(subject, email, from_email, [user.email], html_message=html_email)
            from django.core.mail import EmailMultiAlternatives
            msg = EmailMultiAlternatives(subject, plain_text_content, from_email, [user.email])
            msg.attach_alternative(html_email, "text/html")
            msg.send()



class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ("rut", "first_name", "last_name", "email", "is_superuser", "password", "contact_phone", "group")

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields["first_name"].label = "Nombre"
        self.fields["last_name"].label = "Apellido"
        self.fields["email"].label = "Email"
        self.fields["contact_phone"].label = "Teléfono"
        self.fields["rut"].label = "Rut"
        self.fields["group"].label = "Perfil Administrador"
        self.fields["rut"].widget.attrs["placeholder"] = "Ingrese sólo números. Si su RUT termina en k, reemplace por un 0."
        self.fields["rut"].widget.attrs["maxlength"] = "9"


class UserFormUpdate(forms.ModelForm):

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "is_superuser", "contact_phone")

    def __init__(self, *args, **kwargs):
        super(UserFormUpdate, self).__init__(*args, **kwargs)
        self.fields["first_name"].label = "Nombre"
        self.fields["last_name"].label = "Apellido"
