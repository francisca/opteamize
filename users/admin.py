# -*- encoding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import User


class UserCreationForm(forms.ModelForm):
	
	password1 = forms.CharField(label="Password",
								widget=forms.PasswordInput)
	password2 = forms.CharField(label="Password confirmation",
								widget=forms.PasswordInput)

	class Meta:
		model = User
		fields = ("email",)

	def clean_password2(self):
		"""
		Check passwords match
		"""
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")

		if password1 and password2 and password1 != password2:
			msg = "Password don't match"
			raise forms.ValidationError(msg)

		return password2

	def save(self, commit=True):

		user = super(UserCreationForm, self).save(commit=False)
		user.set_password(self.cleaned_data["password1"])
		if commit:
			user.save()

		return user


class UserChangeForm(forms.ModelForm):

	password = ReadOnlyPasswordHashField()

	class Meta:
		model = User
		fields = ("password",)

	def clean_password(self):

		return self.initial["password"]


class CustomUserAdmin(UserAdmin):

	add_form = UserCreationForm
	form = UserChangeForm
	list_display = ("email", "is_staff", )
	list_filter = ("is_staff", "is_superuser", "is_active", )
	#exclude = ("username", )
	search_fields = ("email",)
	ordering = ("email",)

	fieldsets = (
		(None, {"fields": ("first_name",
			"last_name", "email", "password",
			"section")}),
		("Permissions", {"fields": ("is_active",
									"is_superuser",
									"groups",
									"user_permissions")}),
		("Important dates", {"fields": ("last_login", )}),
	)

	add_fieldsets = (
		(None, {
			"classes": ("wide", ),
			"fields": ("email","first_name","last_name", "password1", "password2")}
		),
	)



admin.site.register(User, CustomUserAdmin)