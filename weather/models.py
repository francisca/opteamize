from __future__ import unicode_literals

from django.db import models

class Weather(models.Model):
	date = models.DateField()
	max_temp = models.IntegerField(default=0)
	min_temp = models.IntegerField(default=0)
	weather_code = models.IntegerField(default=0)
	weather_desc = models.CharField(max_length=50, default='')

	def __unicode__(self):
		return "%s" % self.date

	class Meta:
		app_label = "weather"
		db_table = "weather"
