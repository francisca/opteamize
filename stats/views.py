from django.shortcuts import render
from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin


class StatsView(LoginRequiredMixin, TemplateView):
	template_name = 'stats/index.html'
