# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from users.models import User

class Contact(models.Model):
    name = models.CharField(max_length=100)
    phones = models.CharField(max_length=200)
    contact_type = models.IntegerField(default=1)
    visible = models.IntegerField(default=1)

    def __unicode__(self):
        return "%s" % self.name

    class Meta:
        app_label = 'contact'
        db_table = 'contact'

class ContactUser(models.Model):
    user = models.ForeignKey(User, null=True)
    contact = models.ForeignKey(Contact, null=True)

    def __unicode__(self):
        return "%s" % self.contact

    class Meta:
        app_label = 'contact'
        db_table = 'contact_user'