# -*- encoding: utf-8 -*-
from django import forms
from django.core.validators import URLValidator, RegexValidator

from crispy_forms.helper import FormHelper

from contact.models import (
	Contact
	)


class ContactForm(forms.ModelForm):

	class Meta:
		model = Contact
		fields = ('name', 'phones', 'contact_type')

	def __init__(self, *args, **kwargs):
		super(ContactForm, self).__init__(*args, **kwargs)
		self.fields["name"].label = "Nombre"
		self.fields["phones"].label = u"Teléfono 1"
		self.fields['contact_type'].widget = forms.HiddenInput()
		self.fields["phones"].widget.attrs["maxlength"] = "12"
		self.fields["phones"].widget.attrs["class"] = "onlyNumbers"

		self.helper = FormHelper()
		self.helper.form_id = 'contact_form'
