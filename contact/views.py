from django.shortcuts import render

from django.views.generic import (
	TemplateView,
	FormView)

from contact.forms import ContactForm

from braces.views import LoginRequiredMixin

class ContactListView(LoginRequiredMixin, TemplateView):
	template_name = 'contact/index.html'
	login_url = '/'

class ContactCreateView(LoginRequiredMixin, FormView):
	template_name = 'contact/add.html'
	login_url = '/'
	form_class = ContactForm


class ContactUpdateView(LoginRequiredMixin, FormView):
	template_name = 'contact/update.html'
	login_url = '/'
	form_class = ContactForm

	def get_context_data(self, *args, **kwargs):
		context = super(ContactUpdateView, 
			self).get_context_data(*args, **kwargs)
		context["contact_id"] = self.kwargs["pk"]
		return context