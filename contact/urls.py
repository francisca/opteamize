# -*- encoding: utf-8 -*-
from django.conf.urls import url

from contact.views import (
	ContactListView,
	ContactCreateView,
	ContactUpdateView
	)

urlpatterns = [
	url(r'^$', ContactListView.as_view(), name='contact_list'),
	url(r'^add/$', ContactCreateView.as_view(), name='contact_add'),
   	url(r'^(?P<pk>\d+)/update/$', ContactUpdateView.as_view(), name='contact_update'),
]