# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.db import models

class Level(models.Model):
	name = models.CharField(max_length=200)

	def __unicode__(self):
		return "%s" % self.name

	class Meta:
		app_label = 'level'
		db_table = 'level'


class Evaluation(models.Model):
	name = models.CharField(max_length=200)

	def __unicode__(self):
		return "%s" % self.name

	class Meta:
		app_label = 'level'
		db_table = 'evaluation'

