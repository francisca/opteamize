from django.contrib import admin
from level.models import (
	Level,
	Evaluation
	)

admin.site.register(Level)
admin.site.register(Evaluation)
