# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.db import models
import math

class University(models.Model):
	name = models.CharField(max_length=200)

	def __unicode__(self):
		return "%s" % self.name

	class Meta:
		app_label = 'university'
		db_table = 'university'

