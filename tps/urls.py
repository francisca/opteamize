"""tps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include, patterns
from django.contrib import admin
from django.contrib.auth.views import logout

from tps.views import AuthView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('api.urls')),
    url(r'^$', AuthView.as_view(), name='login'),
    url(r'^user/', include('users.urls')), # CMS
    url(r'^postulant/', include('postulant.urls')), # CMS
    url(r'^contact/', include('contact.urls')),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(r'^stats/', include('stats.urls')),
    # Reset Password
    url(r'^passwordreset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'django.contrib.auth.views.password_reset_confirm',
        {'post_reset_redirect': '/user/password/done/'}),

    url(r'^user/password/done/$',
        'django.contrib.auth.views.password_reset_complete'),
]

from django.conf import settings

urlpatterns += patterns('',
   (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
   'document_root': settings.MEDIA_ROOT}))
