var totalImages = 0
var allowed = ["jpeg", "png"];
var _URL = window.URL || window.webkitURL;
var messageId = $("#message_id").val();

$(function(){

	swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        spaceBetween: 30,
	        direction: 'horizontal',
      		loop: true,
	    });

	$('.swipebox').swipebox({
		loopAtEnd: true
	});

	getNewImages(messageId);
	
	$("#btn_confirm_delete").click(function(){
		deleteImage($(this).attr("data-uri"));
	})

	$(':file').on('fileselect', function(event, numFiles, label){
		$("#alert_error_message").hide('fast');
		var input = $(this).parents('.input-group').find(':text'),
			  log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) {
				input.val(log);
			} else {
				if( log ) alert(log);
			}

			var input = $(this);
			var found = false;
			var size_ok = false;
			var minmax_ok = false;

			var file = input.get(0).files[0];
			img = new Image();
			var imgwidth = 0;
			var imgheight = 0;
			var maxwidth = 1025;
			var minwidth = 481;
			var maxheight = 1025;
			var minheight = 481;
			img.src = _URL.createObjectURL(file);

			allowed.forEach(function(extension) {
			  if (input.get(0).files[0].type.match('image/'+extension)) {
			    	found = true;
			  }
			})

			if(found==false){
				alertErrorMessage("Las imagen debe estar en formato jpg o png");
			}else{

				if(input.get(0).files[0].size<1000000){
					size_ok = true
				}

				if(size_ok==false){
					alertErrorMessage("El tamaño de la imagen debe ser máximo 1mb");
				}else{

					img.onload = function() {
						imgwidth = this.width;
						imgheight = this.height;

						if(imgwidth >= minwidth 
							&& imgwidth <= maxwidth 
							&& imgheight >= minheight 
							&& imgheight <= maxheight){

							minmax_ok = true;

						}

						if(minmax_ok){

							uploadNewImage()

						}else{

							alertErrorMessage("Las dimensiones de la imagen deben ser mínimo 480x480 y máximo 1024x1024");

						}
					};
				}
			}
	})

})

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function getNewImages(messageId){
	$.ajax({
		type : "GET",
		url : "/api/v1/message_image/?message="+messageId, 
		async: false
	})
	.done(function(images){
		totalImages = images["meta"]["total_count"];

		if (totalImages >= 5){
			$("#new_image_form").hide();
			$("#alert_total").show();
			$('.swiper-container').show();
		} else if(totalImages == 0){
			$('.swiper-container').hide();
		} else {
			$('.swiper-container').show();
		}

		$("#total_images").text(totalImages);
		image_name = "";
		$.each(images["objects"], function(i, image){

			image_name = image["image"];
			image_name = image_name.replaceAll(/\//,"_").replaceAll(/\./,"_").replaceAll(/\(|\)/,"")+totalImages;

		    var par = document.createElement('div');
		    par.className = "swiper-slide";
		    var swipebox = document.createElement('a');
		    swipebox.className = "swipebox";
		    swipebox.href = image["image"];
		    par.appendChild(swipebox);
		    var delete_btn = document.createElement('button');
		    delete_btn.className = "btn-trash-image";
		    par.appendChild(delete_btn);
		    var delete_icon = document.createElement('i');
		    delete_icon.className = 'fa fa-trash';
		    delete_icon.style = "color: white;font-size: 26px;"
		    delete_btn.appendChild(delete_icon);
			var img = document.createElement('img');
			img.src = image["image"];
			img.style = "width:100%"
			swipebox.appendChild(img);

		    swiper.appendSlide(par);

		    $('.btn-trash-image')
		    	.attr('data-uri', image["resource_uri"])
		    	.attr('data-target', '#deleteImage')
		    	.attr('data-toggle', 'modal');

		})

		$(".btn-trash-image").click(function(){
			$("#btn_confirm_delete")
				.attr("data-uri", $(this).attr("data-uri"));
		})

	})
	.fail(function(e){
		console.log(e)
	})
}

function alertMessage(totalImages, message){

	document.getElementById("alert_total_message").innerHTML = message;

	if (totalImages < 5){
		$("#new_image_form").show();
		$("#alert_total").hide('fast');
	}
	else{
		$("#new_image_form").hide();
		$("#alert_total").show('fast');	
	}
}

function alertErrorMessage(message){

	document.getElementById("alert_error_message").innerHTML = message;

	$("#alert_error_message").show('fast');

}

function deleteImage(resource_uri){
	// Manejamos el evento de eliminar imagen
	$.ajax({
		type : "DELETE",
		url : resource_uri,
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
        }
	})
	.done(function(response){
		//console.log("Imagen eliminada exitosamente")
		totalImages = totalImages - 1
		$("#total_images").text(totalImages)	
		alertMessage(totalImages, "* Recuerde que puede subir un máximo de 5 imágenes por mensaje");
		$('.swiper-slide').remove();
		getNewImages(messageId);
	})
	.fail(function(err){
		console.log(err)
	})
}

function uploadNewImage(){
	$("#loader").show();
	var form = new FormData();
	form.append("message", "/api/v1/message/"+$("#message_id").val()+"/");
	form.append("image", $("#new_image")[0].files[0]);
	form.append("", "");

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "/api/v1/message_image/",
		"method": "POST",
		"headers": {
			"X-CSRFToken": $('input[name=csrfmiddlewaretoken]').val(),
			// "authorization": "ApiKey 9.276.682-K:dfa6f23a8c071f0db2b76948b65bc30edacfb15d",
			"cache-control": "no-cache",
			// "postman-token": "99960a65-d588-35f1-5d67-0fbaa8ebb80c"
		},
		"processData": false,
		"contentType": false,
		"mimeType": "multipart/form-data",
		"data": form
	}

	$.ajax(settings).done(function (image) {
		var newImage = JSON.parse(image);
		image_name = "";
		image_name = newImage["image"];
		image_name = image_name.replaceAll(/\//,"_").replaceAll(/\./,"_").replaceAll(/\(|\)/,"")+totalImages;
		$("#loader").hide();
		totalImages = totalImages + 1
		$("#total_images").text(totalImages)	
		alertMessage(totalImages, "* Recuerde que puede subir un máximo de 5 imágenes por mensaje");
		$('.swiper-slide').remove();
		getNewImages(messageId);
	});

		
}