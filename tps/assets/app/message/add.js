var totalImages = 0
var allowed = ["jpeg", "png"];
var _URL = window.URL || window.webkitURL;

$(function(){

	$("#id_title").addClass("tastypie_error");
	message_id = null;
	inputs = [];

	$(document).on('click', '.btn-trash-image', function(){
		//$(this).closest("div").remove();
		totalImages = totalImages - 1;
		$("#total_images").text(totalImages);
		alertMessage(totalImages, "* Recuerde que puede subir un máximo de 5 imágenes por mensaje");
		inputs.splice($(this).attr('data-id'), 1);
		getImages();
	})

	$(':file').on('fileselect', function(event, numFiles, label){
		var input = $(this).parents('.input-group').find(':text'),
			  log = numFiles > 1 ? numFiles + ' files selected' : label;

			if( input.length ) {

				$("#loader").hide();
				$("#"+label.split(".")[0].replaceAll(" ","_").replaceAll(/\(|\)/,"")+totalImages).click(function(){
						var removeItemEl = $(this);
						totalImages = totalImages - 1;
						$("#total_images").text(totalImages);
						removeItemEl.closest("tr").remove();
						alertMessage(totalImages, "* Recuerde que puede subir un máximo de 5 imágenes por mensaje");
					
				})

				totalImages = totalImages + 1
				$("#total_images").text(totalImages)
				alertMessage(totalImages, "* Recuerde que puede subir un máximo de 5 imágenes por mensaje");

			} else {
				if( log ) alert(log);
			}
	})

	$(document).on('change', ':file', function() {
			$("#alert_error_message").hide('fast');
			$(".swiper-container").show();
			$("#alert_total").hide();

			var input = $(this);
			var found = false;
			var size_ok = false;
			var minmax_ok = false;

			var file = input.get(0).files[0];
			img = new Image();
			var imgwidth = 0;
			var imgheight = 0;
			var maxwidth = 1025;
			var minwidth = 481;
			var maxheight = 1025;
			var minheight = 481;
			img.src = _URL.createObjectURL(file);

			allowed.forEach(function(extension) {
			  if (input.get(0).files[0].type.match('image/'+extension)) {
			    	found = true;
			  }
			})

			if(found==false){
				alertErrorMessage("Las imagen debe estar en formato jpg o png");
			}else{

				if(input.get(0).files[0].size<1000000){
					size_ok = true
				}

				if(size_ok==false){
					alertErrorMessage("El tamaño de la imagen debe ser máximo 1mb");
				}else{

					img.onload = function() {
						imgwidth = this.width;
						imgheight = this.height;

						if(imgwidth >= minwidth 
							&& imgwidth <= maxwidth 
							&& imgheight >= minheight 
							&& imgheight <= maxheight){

							minmax_ok = true;
						}

						if(minmax_ok){

							input,
					        numFiles = input.get(0).files ? input.get(0).files.length : 1,
					        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');

					        inputs.push(input.get(0).files[0]);

					        getImages();

					    	input.trigger('fileselect', [numFiles, label]);

						}else{

							alertErrorMessage("Las dimensiones de la imagen deben ser mínimo 480x480 y máximo 1024x1024");
							
						}
					};
				}
			}

	});
	
	// Función que se encarga de generar un nuevo Mensaje
	$("#message_btn").click(function(e){
		$("#message_btn").text("Cargando datos...");
		e.preventDefault();

		var message_data = $("#message_form").serializeObject();
		var resource_uri = "/api/v1/message/";

		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": resource_uri,
		  "method": "POST",
		  "headers": {
		    "cache-control": "no-cache",
		    "X-CSRFToken": $('input[name=csrfmiddlewaretoken]').val()
		  },
		  "processData": false,
		  "contentType": "application/json",
		  "data": JSON.stringify(message_data)
		}
		$.ajax(settings)
			.done(function (response) {
			  	$(".form-validation").remove();
				//$("#message_btn").text("Agregar nuevo Mensaje");
				$("#alert_ok").show();
				$("#alert_error").hide();

			    message_id = response.id;
			    document.getElementById("message_id").value = message_id;

			    message = {
			    			title:response.title,
			    			container:"",
						  	notification_type:"M",
						  	date:"",
						  	slot_stacking:"",
						  	resource_uri:response.resource_uri,
						  	message_id:message_id
			    		}
			    index = 0;
			    inputs.forEach( function (input){

			    	uploadNewImage(input);
			    	index += 1;
					
	            });

			    var settings = {
				  "async": true,
				  "crossDomain": true,
				  "url": "/api/v1/change_status/",
				  "method": "POST",
				  "headers": {
				    "cache-control": "no-cache",
				    "X-CSRFToken": $('input[name=csrfmiddlewaretoken]').val()
				  },
				  "processData": false,
				  "contentType": "application/json",
				  "data": JSON.stringify(message)
				}
				$.ajax(settings)
					.done(function (response) {
					  	
					  	window.setTimeout(function(){
					        window.location.href = document.referrer;
					    }, 1000);

					})
					.fail(function(e){
						console.log(e);
					})

			})
			.fail(function(e){
				console.log(e);
				$(".form-validation").remove();
				$("form :input")
					.css('border', '')
					.css('background-color', '');
				$("#message_btn").text("Agregar nuevo Mensaje");
				$("#alert_ok").hide();
				$("#alert_error").show();
				errors = JSON.parse(e["responseText"]);
				$.each(errors["message"], function(field, error){
					$("#id_"+field)
						.css('border', '1px solid #a94442')
						.css('background-color', '#f2dede')
						.after("<span class='form-validation' style='color:red'>"+error+" </span>");
				})
			})
	});

	$('.message-link').addClass('active-link');
});

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function alertMessage(totalImages, message){

	document.getElementById("alert_total_message").innerHTML = message;

	if (totalImages < 5){
		$("#new_image_form").show();
		$("#alert_total").hide('fast');
		if (totalImages == 0) {
			$('.swiper-container').hide();
		} else {
			$('.swiper-container').show();
		}
	}
	else{
		$("#new_image_form").hide();
		$("#alert_total").show('fast');	
	}
}

function alertErrorMessage(message){

	document.getElementById("alert_error_message").innerHTML = message;

	$("#alert_error_message").show('fast');
	
}


function getImages(){
	console.log(totalImages);
	$('.swiper-slide').remove();
	$.each(inputs, function(i, input){
		var row = "<div class='swiper-slide'>"+
	    	"<button class='btn-trash-image' data-id="+i+"><i class='fa fa-trash' style='color:white;font-size:26px;'></i></button>"+
			"<a class='swipebox' href="+_URL.createObjectURL(input)+">"+
			"<img src="+_URL.createObjectURL(input)+" style='width:100%'></img>"+
			"</a>"+
			"</div>";
		$(".swiper-wrapper").prepend(row);
	})
	swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        paginationClickable: true,
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        spaceBetween: 30,
	        direction: 'horizontal',
      		loop: true,
	    });

	$('.swipebox').swipebox({
		loopAtEnd: true
	});
}
function uploadNewImage(input){
	$("#loader").show();
	var form = new FormData();
	form.append("message", "/api/v1/message/"+$("#message_id").val()+"/");
	form.append("image", input);
	form.append("", "");

	var settings = {
		"async": true,
		"crossDomain": true,
		"url": "/api/v1/message_image/",
		"method": "POST",
		"headers": {
			"X-CSRFToken": $('input[name=csrfmiddlewaretoken]').val(),
			// "authorization": "ApiKey 9.276.682-K:dfa6f23a8c071f0db2b76948b65bc30edacfb15d",
			"cache-control": "no-cache",
			// "postman-token": "99960a65-d588-35f1-5d67-0fbaa8ebb80c"
		},
		"processData": false,
		"contentType": false,
		"mimeType": "multipart/form-data",
		"data": form
	}

	$.ajax(settings).done(function (image) {
		
	});

		
}