$(function(){
	getMessage();
	$("#message_btn").click(function(e){
		updateMessage(e);
	});
	$('.message-link').addClass('active-link');
});

function getMessage() {
	var message_id = $("#message_id").val()
	$.ajax({
		type : "GET",
		url : "/api/v1/message/"+message_id+"/"
	})
	.done(function(message){
		$.each(message, function(key, value){
			$("#id_"+key).val(value);
		})
		$("#ntitle").text(message["title"])
	})
	.fail(function(e){
		console.log(e);
	})
}

function updateMessage(e){
	e.preventDefault();
	var message_data = $("#message_form").serializeObject();
	var message_id = $("#message_id").val();
	$.ajax({
		type : "PUT",
		url : "/api/v1/message/"+message_id+"/",
		contentType : "application/json",
		data : JSON.stringify(message_data),
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
        }
	}).done(function(message_info){
		// mostramos mensaje de éxito
	  	$(".form-validation").remove();
		$("#message_btn").text("Agregar Nuevo Mensaje");
		$("#success_message").show();
		$("#alert_error").hide();

		window.setTimeout(function(){
	        window.location.href = document.referrer;
	    }, 1000);
	    
	}).fail(function(e){
		$(".form-validation").remove();
		$("#success_message").hide();
		$("form :input")
			.css('border', '')
			.css('background-color', '');
		$("#message_btn").text("Agregar Nuevo Mensaje");
		$("#alert_ok").hide();
		$("#alert_error").show();
		errors = JSON.parse(e["responseText"]);
		$.each(errors["message"], function(field, error){
			console.log("#id_"+field);
			$("#id_"+field)
				.css('border', '1px solid #a94442')
				.css('background-color', '#f2dede')
				.after("<span class='form-validation' style='color:red'>"+error+" </span>");
		})
	})
}