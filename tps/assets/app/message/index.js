$(function(){
	getMessage("/api/v1/message/");
	$(".page").click(function(){
		getMessage($(this).attr("data-url"));
	})
	$("#btn_confirm_delete").click(function(){
		console.log($(this).attr("data-uri"));
		deleteMessage($(this).attr("data-uri"),$(this).attr("data-id") );
	})
	$('.message-link').addClass('active-link');
})

function getMessage(resource_uri){
	$.ajax({
		type : "GET",
		url : resource_uri
	})
	.done(function(messages){
		
		var rows = "";
		$.each(messages["objects"], function(i, message){
			var btn_update = "<a href='/message/"+message["id"]+"/update/' class='btn btn-lg btn-icon btn-info'><i class='fa fa-edit'></i></a>";
			var btn_delete = "<a href='#!'  data-toggle='modal' data-id='"+message["id"]+"' data-uri='"+message["resource_uri"]+"' data-target='#deleteMessage' class='btn btn-lg btn-icon btn-danger btn-delete-message'><i class='fa fa-trash'></i></a>"
			var row = "<tr>"+
				"<td>"+message["title"]+"</td>"+
				"<td><div class='text-ellipsis'>"+message["message"]+"</div></td>"+
				"<td>"+format_datetime(message["date"])+"</td>"+
				"<td>"+btn_update+"&nbsp;&nbsp;"+btn_delete+"</td>"+
				"</tr>";
			rows += row;
		});

		$("#message_table tbody").html(rows);
		set_pagination_results(messages);

		$(".btn-delete-message").click(function(){
			$("#btn_confirm_delete")
				.attr("data-uri", $(this).attr("data-uri"))
				.attr("data-id", $(this).attr("data-id"));
		})
	})
	.fail(function(e){
		console.log(e);
	})
}

function deleteMessage(resource_uri, id){
    $.ajax({
        type: "GET",
        url: resource_uri,
        success: function(message){
            message["visible"] = 0;
                $.ajax({
                    type: "PUT",
                    url: "/api/v1/message_update/"+id+"/",
                    contentType: "application/json",
                    data: JSON.stringify(message),
                    beforeSend: function(jqXHR, settings) {
                        jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                    },
                    /*success: function(delete_message){
                        getMessage("/api/v1/message/");
                    },
                    error: function(error){
                        console.log(error);
                    }*/
                })
                .done(function(admin){
					getMessage("/api/v1/message/");
				})
				.fail(function(e){
					console.log(e);
				})
        },
        error: function(message){
            console.log(message);
        }
    });
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getMessage(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#message_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getMessage(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#message_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}