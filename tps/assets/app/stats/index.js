$(document).on("ready", function(){
    
    $("html, body").delay(100).animate({scrollTop: $('#box_chart_index').offset().top }, 400);

    travels = [];
    waiting_time_travels = [];

    initial_dates();

    $('input[name="waitChart_filter_date"]').daterangepicker({
          "format": 'YYYY-MM-DD',
          locale:{
            applyLabel: 'Seleccionar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta'
          },
          maxDate: new Date()
        });

    getUserGroup();
    get_arrivalChart();
    get_waitChart($("#waitChart_filter_date").val());
    
    $("#waitChart_btn_filter").on("click", function(){
        waiting_time_travels = [];
        get_waitChart($("#waitChart_filter_date").val());
    });

    $('.stats-link').addClass('active-link');
});


function getUserGroup(){
    $.ajax({
        type : 'GET',
        url : "/api/v1/user_location/?limit=0",
    })
}
function initial_dates(){

    start_at = format();    
    finish_at = format();

    $("#waitChart_filter_date").val(start_at+" - "+finish_at);
}

function getUserLocation(){

    var labels = [];
    var data_info = [];
    var color_list = [
        "#ffccbc",
        "#4fc3f7",
        "#ffe57f",
        "#f4ff81",
        "#cddc39",
        "#81c784",
        "#b9f6ca",
        "#f57c00",
        "#9575cd",
        "#f48fb1",
        "#ab47bc",
        "#1e88e5",
        "#22aee4",
        "#e16924",
        "#dbafbf",
        "#e0b71a",
        "#079702"
    ];

    $.ajax({
        type : 'GET',
        url : "/api/v1/user_location/?limit=0",
    })
    .done(function(results){

        var oneorless = 0;
        var onetwo = 0;
        var twofour = 0;
        var foursix = 0;
        var sixeigth = 0;
        var teentwelve = 0;
        var moretwelve = 0;
        var moretwentyfour = 0;
        var userLocations = [];
        info_list = ""

        $.each(results["objects"], function(k, user_location){
            userLocations.push(user_location);
        });

        travels.forEach( function (travelItem){
            /*console.log(travelItem);*/

            userLocations.forEach( function (userLocItem){

                if(travelItem["id"]==userLocItem["travel"]["id"] && travelItem["user"]["id"]==userLocItem["user"]["id"]){

                    if(userLocItem["eta"]<3600){
                        oneorless += 1;
                    }
                    if(userLocItem["eta"]>=3600 && userLocItem["eta"]<=7200){
                        onetwo += 1;
                    }
                    if(userLocItem["eta"]>7200 && userLocItem["eta"]<=14400){
                        twofour += 1;
                    }
                    if(userLocItem["eta"]>14400 && userLocItem["eta"]<=21600){
                        foursix += 1;
                    }
                    if(userLocItem["eta"]>21600 && userLocItem["eta"]<=28800){
                        sixeigth += 1;
                    }
                    if(userLocItem["eta"]>28800 && userLocItem["eta"]<=43200){
                        teentwelve += 1;
                    }
                    if(userLocItem["eta"]>43200 && userLocItem["eta"]<=86400){
                        moretwelve += 1;
                    }
                    if(userLocItem["eta"]>86400){
                        moretwentyfour += 1;
                    }
                }
            });
        });

        labels.push("- 1 hr.");
        labels.push("1-2 hrs.");
        labels.push("2-4 hrs.");
        labels.push("4-6 hrs.");
        labels.push("6-8 hrs.");
        labels.push("8-12 hrs.");
        labels.push("12-24 hrs.");
        labels.push("+ 24 hrs.");


        data_info.push(oneorless);
        data_info.push(onetwo);
        data_info.push(twofour);
        data_info.push(foursix);
        data_info.push(sixeigth);
        data_info.push(teentwelve);
        data_info.push(moretwelve);
        data_info.push(moretwentyfour);

        //var total = 0;                
        for(var k in labels) {
            //total = parseInt(total) + parseInt(data_info[k])
            info_row = "<tr>"+
                "<td width='20px' style='background-color:"+color_list[k]+"'></td>"+
                "<td>"+ labels[k] +"</td>"+
                "<td><strong>"+ data_info[k] +"</strong></td>"+
                "</tr>";
            info_list += info_row;
        }

        $("#arrivalChart_table tbody").html(info_list);

        var data = {
                    labels: labels,
                    datasets: [
                        {
                            data: data_info,
                            backgroundColor: color_list
                        }]
                };

        $('.arrival-box-gif').css('display','none');

        var ctx = document.getElementById("arrivalChart");
        var myDoughnutChart = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                legend: {
                    display: false
                },
                animation: {
                    animateScale: true
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            callback: function (value) { if (Number.isInteger(value)) { return value; } },
                            stepSize: 1
                        }
                    }]
                }
            }
        });

    })
    .fail(function(e){

    })
}


function get_arrivalChart(){
    
    $.ajax({
        type : "GET",
        url : "/api/v1/travel_list/?limit=0&finish_type=0",
        success: function(results){
            /*console.log(results);*/
            if (results["meta"]["total_count"] == 0){
                var labels = [];
                var data_info = [];
                var color_list = [
                    "#ffccbc",
                    "#4fc3f7",
                    "#ffe57f",
                    "#f4ff81",
                    "#cddc39",
                    "#81c784",
                    "#b9f6ca",
                    "#f57c00",
                    "#9575cd",
                    "#f48fb1",
                    "#ab47bc",
                    "#1e88e5",
                    "#22aee4",
                    "#e16924",
                    "#dbafbf",
                    "#e0b71a",
                    "#079702"
                ];
                labels.push("- 1 hr.");
                labels.push("1-2 hrs.");
                labels.push("2-3 hrs.");
                labels.push("3-4 hrs.");
                labels.push("4-5 hrs.");
                labels.push("5-6 hrs.");
                labels.push("6-7 hrs.");
                labels.push("7-8 hrs.");
                labels.push("+ 8 hrs.");

                info_list = "";
                for(var k in labels) {
                    info_row = "<tr>"+
                        "<td width='20px' style='background-color:"+color_list[k]+"'></td>"+
                        "<td>"+ labels[k] +"</td>"+
                        "<td><strong>0</strong></td>"+
                        "<td><strong>0</strong></td>"+
                        "</tr>";
                    info_list += info_row;
                }

                $("#arrivalChart_table tbody").html(info_list);
                $('#no-register-graph-arrival').css('display','block');
                $('.arrival-box-gif').css('display','none');
            }
            else{
                $.each(results["objects"], function(k, travel){
                    
                        travels.push(travel);
                });

                getUserLocation();

            }
        }
    });

};

function getWaitingTimeUserLocation(init_date, finish){

    var labels = [];
    var data_info = [];
    var color_list = [
        "#ffccbc",
        "#4fc3f7",
        "#ffe57f",
        "#f4ff81",
        "#cddc39",
        "#81c784",
        "#b9f6ca",
        "#f57c00",
        "#9575cd",
        "#f48fb1",
        "#ab47bc",
        "#1e88e5",
        "#22aee4",
        "#e16924",
        "#dbafbf",
        "#e0b71a",
        "#079702"
    ];

    travel_id_list = ""

    waiting_time_travels.forEach( function (travelItem){

        travel_id_list = travel_id_list + "&travel__in=" + travelItem["id"];
    });

    resource_uri = "/api/v1/user_location_list/?limit=0&order_by=timestamp&travel_step=2"+travel_id_list;

    $.ajax({
        type : 'GET',
        url : resource_uri,
    })
    .done(function(results){

        var oneorless = 0;
        var onetwo = 0;
        var twothree = 0;
        var threefour = 0;
        var fourfive = 0;
        var fivesix = 0;
        var sixseven = 0;
        var seveneight = 0;
        var moreeight = 0;
        var userLocations = [];
        info_list = ""

        $.each(results["objects"], function(k, user_location){
                
                userLocations.push(user_location);
        });

        waiting_time_travels.forEach( function (travelItem){

            var first = true;
            var first_date;
            var waitEta = 0;

            userLocations.forEach( function (userLocItem){

                if(travelItem["id"]==userLocItem["travel"]["id"] && travelItem["user"]["id"]==userLocItem["user"]["id"]){

                    if(first==true){
                        first_date = userLocItem["timestamp"];
                        first = false;
                    }else{

                        var date1 = new Date(first_date.replace(/-/g,'/').replace("T"," ").split(".")[0]);
                        var date2 = new Date(userLocItem["timestamp"].replace(/-/g,'/').replace("T"," ").split(".")[0]);

                        waitEta = Math.abs(date2.getTime() - date1.getTime())/1000;
                    }

                }

            });

            if(first==false){

                if(waitEta<3600){
                    oneorless += 1;
                }
                if(waitEta>=3600 && waitEta<=7200){
                    onetwo += 1;
                }
                if(waitEta>7200 && waitEta<=10800){
                    twothree += 1;
                }
                if(waitEta>10800 && waitEta<=14400){
                    threefour += 1;
                }
                if(waitEta>14400 && waitEta<=18000){
                    fourfive += 1;
                }
                if(waitEta>18000 && waitEta<=21600){
                    fivesix += 1;
                }
                if(waitEta>21600 && waitEta<=25200){
                    sixseven += 1;
                }
                if(waitEta>25200 && waitEta<=28800){
                    seveneight += 1;
                }
                if(waitEta>28800){
                    moreeight += 1;
                }

            }
        });

        labels.push("- 1 hr.");
        labels.push("1-2 hrs.");
        labels.push("2-3 hrs.");
        labels.push("3-4 hrs.");
        labels.push("4-5 hrs.");
        labels.push("5-6 hrs.");
        labels.push("6-7 hrs.");
        labels.push("7-8 hrs.");
        labels.push("+ 8 hrs.");

        data_info.push(oneorless);
        data_info.push(onetwo);
        data_info.push(twothree);
        data_info.push(threefour);
        data_info.push(fourfive);
        data_info.push(fivesix);
        data_info.push(sixseven);
        data_info.push(seveneight);
        data_info.push(moreeight);

        for(var k in labels) {
            info_row = "<tr>"+
                "<td width='20px' style='background-color:"+color_list[k]+"'></td>"+
                "<td>"+ labels[k] +"</td>"+
                "<td><strong>"+ data_info[k] +"</strong></td>"+
                "</tr>";
            info_list += info_row;
        }

        $("#waitChart_table tbody").html(info_list);

        var data = {
                    labels: labels,
                    datasets: [
                        {
                            data: data_info,
                            backgroundColor: color_list
                        }]
                };

        $('.wait-box-gif').css('display','none');

        var ctx_wait = document.getElementById("waitChart");

        flag_no_info = true;

        data_info.forEach( function (item){
            if(item>0){
                flag_no_info = false;
            }
        });

        if(flag_no_info==true){
            default_chart();
        } else {
            var myDoughnutChart = new Chart(ctx_wait, {
                type: 'bar',
                data: data,
                options: {
                    legend: {
                        display: false
                    },
                    animation: {
                        animateScale: true
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                callback: function (value) { if (Number.isInteger(value)) { return value; } },
                                stepSize: 1
                            }
                        }]
                    }
                }
            });
        }

    })
    .fail(function(e){
        default_chart();
    })
}

function get_waitChart(filter_date){

    $('.wait-box-gif').css('display','block');
    
    range_date = filter_date.split(" - ");

    init_date = range_date[0]+" 00:00:00";
    finish = range_date[1]+" 23:59:59";

    url = "/api/v1/travel_list/?limit=0&init_date__range="+init_date+","+finish;

    init_date = range_date[0]+" 00:00:00";
    finish = range_date[1]+" 23:59:59";

    $.ajax({
        type : "GET",
        url : url,
        success: function(results){
            if (results["meta"]["total_count"] == 0){

                $('.wait-box-gif').css('display','none');

                default_chart();
            }
            else{   
                $('#waitChart').css('display','block');
                $('#no-register-graph-waiting').css('display','none');

                $.each(results["objects"], function(k, travel){
                    if(travel.finish_date!='' && travel.finish_type==1){
                        waiting_time_travels.push(travel);
                    }
                });

                getWaitingTimeUserLocation(init_date, finish);

            }
        }
    });

};

function default_chart(){

var labels = [];
var data_info = [];
var color_list = [
    "#ffccbc",
    "#4fc3f7",
    "#ffe57f",
    "#f4ff81",
    "#cddc39",
    "#81c784",
    "#b9f6ca",
    "#f57c00",
    "#9575cd",
    "#f48fb1",
    "#ab47bc",
    "#1e88e5",
    "#22aee4",
    "#e16924",
    "#dbafbf",
    "#e0b71a",
    "#079702"
];

labels.push("- 1 hr.");
labels.push("1-2 hrs.");
labels.push("2-3 hrs.");
labels.push("3-4 hrs.");
labels.push("4-5 hrs.");
labels.push("5-6 hrs.");
labels.push("6-7 hrs.");
labels.push("7-8 hrs.");
labels.push("+ 8 hrs.");

info_list = "";
for(var k in labels) {
    info_row = "<tr>"+
        "<td width='20px' style='background-color:"+color_list[k]+"'></td>"+
        "<td>"+ labels[k] +"</td>"+
        "<td><strong>0</strong></td>"+
        "</tr>";
    info_list += info_row;
}

var data = {
            labels: labels,
            datasets: [
                {
                    data: data_info,
                    backgroundColor: color_list
                }]
        };

var ctx = document.getElementById("waitChart");
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: {
        legend: {
            display: false
        }
    }
});

$("#waitChart_table tbody").html(info_list);
$('#waitChart').css('display','none');
$('#no-register-graph-waiting').css('display','block');

}