/*
Serializar formulario en un objecto JSON
*/
$.fn.serializeObject = function(){
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
    }
    o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  return o;
};

/*
Format datetime 
AAAA-MM-DDT:HH:MM:SS.000
*/
function format_datetime(data){
	date = data.split("T");
	day = format_date(date[0]); // Día reporte
	hour = date[1].split("."); // Hora reporte
	return day+" "+hour[0];
}

function format_datetime_basic(data){
    date = data.split("T");
    day = format_date(date[0]); // Día reporte
    return day+" "+date[1];
}

/*
Fomate Date
AAAA-MM-DD
*/
function format_date(data){
	day = data.toString().split("-");
	return day[2]+"/"+day[1]+"/"+day[0];
}


/*
Formate Date with zero
AAAA-MM-DD
*/
function format(){

    var d = new Date();
    year = d.getFullYear();
    month = d.getMonth() +  1;
    day = d.getDate();

    if (month < 10){
        month = "0"+month;
    }
    if (day < 10){
            day = "0"+day;
    }

    return year+"-"+month+"-"+day;
}
/*
*/

/*
FORMAT DURATION 00:00:00:00 
*/
function sformat(s) {
      var fm = [
            Math.floor(s / 60 / 60 / 24), // DAYS
            Math.floor(s / 60 / 60) % 24, // HOURS
            Math.floor(s / 60) % 60, // MINUTES
            s % 60 // SECONDS
      ];
      return $.map(fm, function(v, i) { return ((v < 10) ? '0' : '') + v; }).join(':');
}
/*
*/


/*
FORMATE DURATION 0days 0h 0min
*/
function daysHoursMin(timesecond){
    var duration = "";
    days = timesecond / 60 / 60 / 24;
    hours = (timesecond / 60 / 60) % 24;
    minutes = (timesecond /60) % 60;
    if (days > 1) {
        duration += parseInt(days) + (parseInt(days) > 1 ? 'días ': 'día ');
    }
    if (hours > 1) {
        duration += parseInt(hours) + "h ";
    }
    duration += parseInt(minutes) + "min";
    return duration;
}
/*
*/


/*
VALIDATE PHONES PREFIX +56
*/
function validatePhone(phone){
    if (
        (phone.val().length > 0) && (phone.val().substr(0,3) != '+56')
        || ($(this).val() == '')
        ){
        phone.val('+56'); 
    }
}
/*
*/

/*
VALIDATE ONLY NUMBERS 
*/
function onlyNumbers(e){
   -1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault(); 
}
/*
*/

/*
TimeDate
00:00:00
*/
function secondsToHms(d) {
    d = Number(d);

    /*var pos = d
    var day = pos / (3600*24)
    var rem = pos % (3600*24)
    var hour = rem / 3600
    var rem = rem % 3600
    var mins = rem / 60
    var secs = rem % 60
    res = '%03d:%02d:%02d:%02d' % (day, hour, mins, secs)*/

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}


/* 
RUT VALIDATE
*/
function revisarDigito( dvr )
{   
    dv = dvr + ""   
    if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K') 
    {                 
        return false;   
    }   
    return true;
}

function revisarDigito2( crut )
{   
    largo = crut.length;    
    if ( largo < 2 )    
    {       
        return false;   
    }   
    if ( largo > 2 )        
        rut = crut.substring(0, largo - 1); 
    else        
        rut = crut.charAt(0);   
    dv = crut.charAt(largo-1);  
    revisarDigito( dv );    

    if ( rut == null || dv == null )
        return 0    

    var dvr = '0'   
    suma = 0    
    mul  = 2    

    for (i= rut.length -1 ; i >= 0; i--)    
    {   
        suma = suma + rut.charAt(i) * mul       
        if (mul == 7)           
            mul = 2     
        else                
            mul++   
    }   
    res = suma % 11 
    if (res==1)     
        dvr = 'k'   
    else if (res==0)        
        dvr = '0'   
    else    
    {       
        dvi = 11-res        
        dvr = dvi + ""  
    }
    if ( dvr != dv.toLowerCase() )  
    {       
        return false    
    }

    return true
}

function Rut(texto)
{   
    var tmpstr = "";    
    for ( i=0; i < texto.length ; i++ )     
        if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
            tmpstr = tmpstr + texto.charAt(i);  
    texto = tmpstr; 
    largo = texto.length;   

    if ( largo < 2 )    
    {          
        return false;   
    }   

    for (i=0; i < largo ; i++ ) 
    {           
        if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
        {               
            return false;       
        }   
    }   

    var invertido = ""; 
    for ( i=(largo-1),j=0; i>=0; i--,j++ )      
        invertido = invertido + texto.charAt(i);    
    var dtexto = "";    
    dtexto = dtexto + invertido.charAt(0);  
    dtexto = dtexto + '-';  
    cnt = 0;    

    for ( i=1,j=2; i<largo; i++,j++ )   
    {           
        if ( cnt == 3 )     
        {           
            dtexto = dtexto + '.';          
            j++;            
            dtexto = dtexto + invertido.charAt(i);          
            cnt = 1;        
        }       
        else        
        {               
            dtexto = dtexto + invertido.charAt(i);          
            cnt++;      
        }   
    }   

    invertido = ""; 
    for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )      
        invertido = invertido + dtexto.charAt(i);   

    window.document.form_login.rut.value = invertido.toUpperCase()       

    if (revisarDigito2(texto))      
        return true;    

    return false;
}
/* 
FIN RUT VALIDATE
*/


$(function () {
    $('.list-group.checked-list-box .list-group-item').each(function () {
        
        // Settings
        var $widget = $(this),
            $checkbox = $('<input type="checkbox" class="hidden" />'),
            color = ($widget.data('color') ? $widget.data('color') : "primary"),
            style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };
            
        $widget.css('cursor', 'pointer')
        $widget.append($checkbox);

        // Event Handlers
        $widget.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });
          

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $widget.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $widget.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$widget.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $widget.addClass(style + color + ' active');
            } else {
                $widget.removeClass(style + color + ' active');
            }
        }

        // Initialization
        function init() {
            
            if ($widget.data('checked') == true) {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
            }
            
            updateDisplay();

            // Inject the icon if applicable
            if ($widget.find('.state-icon').length == 0) {
                $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
            }
        }
        init();
    });
    
    $('#get-checked-data').on('click', function(event) {
        event.preventDefault(); 
        var checkedItems = {}, counter = 0;
        $("#check-list-box li.active").each(function(idx, li) {
            checkedItems[counter] = $(li).text();
            counter++;
        });
        $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));
    });
});