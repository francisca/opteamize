function getContact(){
	var contact_id = $("#contact_id").val();
	$.ajax({
		type : "GET",
		url : "/api/v1/contact/"+contact_id+"/"
	
	}).done(function(contact){
		$.each(contact, function(key, value){
			if(key == "phones") {
				var phones = value.split(",");
				$("#id_phones").val(phones[0]);
				$("#id_phone_2").val(phones[1]);
			} else {
				$("#id_"+key).val(value);
			}
		});
	
	}).fail(function(e){
		console.log(e)
	})
}

$(function(){
	getContact();
	$('#id_phones').keyup(function(){
       validatePhone($(this));
    });
    $('#id_phone_2').keyup(function(){
    	validatePhone($(this));
    });

	$("#btn_update").click(function(e){
		e.preventDefault();
		var data = $("#contact_form").serializeObject();
		var contact_id = $("#contact_id").val();
		if ($("#id_phone_2").val() != "" && $("#id_phone_2").val() != "+56") {
			data["phones"] = data["phones"] + "," + $("#id_phone_2").val();
		}
		$.ajax({
			type : "PUT",
			url : "/api/v1/contact/"+contact_id+"/",
			data : JSON.stringify(data),
			contentType : "application/json",
			beforeSend: function(jqXHR, settings) {
	            // Pull the token out of the DOM.
	            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
	        },
		
		}).done(function(contact){
			$("#success_contact").show();
		  	$(".form-validation").remove();
			$("#contact_btn").text("Editar Contacto");
			$("#alert_ok").show();
			$("#alert_error").hide();

		    window.setTimeout(function(){
		        window.location.href = document.referrer;
		    }, 1000);			
		}).fail(function(e){
			$(".form-validation").remove();
			$("form :input")
				.css('border', '')
				.css('background-color', '');
			$("#contact_btn").text("Editar Contacto");
			$("#alert_ok").hide();
			$("#alert_error").show();
			errors = JSON.parse(e["responseText"]);
			$.each(errors["contact"], function(field, error){
				$("#id_"+field)
					.css('border', '1px solid #a94442')
					.css('background-color', '#f2dede')
					.after("<span class='form-validation' style='color:red'>"+error+"</span>");
			})
		})

	})

	$('.contact-link').addClass('active-link');
})