$(function(){
	$("#btn_add").click(function(e){
		addContact(e);
	})

	$('.onlyNumbers').keydown(function(e){onlyNumbers(e)});
    $('.onlyNumbers').keyup(function(){
       validatePhone($(this));
    });

    $('.contact-link').addClass('active-link');
});

function addContact(e){
		$("#contact_btn").text("Cargando datos...");
		e.preventDefault();

		var contact_data = $("#contact_form").serializeObject();

		if ($("#id_phone_2").val() != "" && $("#id_phone_2").val() != "+56") {
			contact_data["phones"] = contact_data["phones"] + "," + $("#id_phone_2").val();
		}

		var resource_uri = "/api/v1/contact/";

		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": resource_uri,
		  "method": "POST",
		  "headers": {
		    "cache-control": "no-cache",
		    "X-CSRFToken": $('input[name=csrfmiddlewaretoken]').val()
		  },
		  "processData": false,
		  "contentType": "application/json",
		  "data": JSON.stringify(contact_data)
		}
		$.ajax(settings)
			.done(function (response) {
				$("#success_contact").show();
			  	$(".form-validation").remove();
				$("#contact_btn").text("Agregar nuevo Contacto");
				$("#alert_ok").show();
				$("#alert_error").hide();

			    window.setTimeout(function(){
			        window.location.href = document.referrer;
			    }, 1000);
			})
			.fail(function(e){
				$(".form-validation").remove();
				$("form :input")
					.css('border', '')
					.css('background-color', '');
				$("#contact_btn").text("Agregar nuevo Contacto");
				$("#alert_ok").hide();
				$("#alert_error").show();
				errors = JSON.parse(e["responseText"]);
				$.each(errors["contact"], function(field, error){
					$("#id_"+field)
						.css('border', '1px solid #a94442')
						.css('background-color', '#f2dede')
						.after("<span class='form-validation' style='color:red'>"+error+"</span>");
				})
			})
}