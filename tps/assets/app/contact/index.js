$(function(){
	getContact("/api/v1/contact/?contact_type=1");
	$(".page").click(function(){
		getContact($(this).attr("data-url"));
	})
	$("#btn_confirm_delete").click(function(){
		deleteContact($(this).attr("data-uri"),$(this).attr("data-id") );
	})
	$('.contact-link').addClass('active-link');
})

function getContact(resource_uri){
	$.ajax({
		type : "GET",
		url : resource_uri
	})
	.done(function(contacts){
		
		var rows = "";
		$.each(contacts["objects"], function(i, contact){
			var btn_update = "<a href='/contact/"+contact["id"]+"/update/' class='btn btn-lg btn-icon btn-info'><i class='fa fa-edit'></i></a>";
			var btn_delete = "<a href='#!'  data-toggle='modal' data-id='"+contact["id"]+"' data-uri='"+contact["resource_uri"]+"' data-target='#deleteContact' class='btn btn-lg btn-icon btn-danger btn-delete-contact'><i class='fa fa-trash'></i></a>";
			var row = "<tr>"+
				"<td>"+contact["name"]+"</td>"+
				"<td>"+contact["phones"]+"</td>"+
				"<td>"+btn_update+"&nbsp;&nbsp;"+btn_delete+"</td>"+
				"</tr>";
			rows += row;
		});

		$("#contact_table tbody").html(rows);
		set_pagination_results(contacts);

		$(".btn-delete-contact").click(function(){
			$("#btn_confirm_delete")
				.attr("data-uri", $(this).attr("data-uri"))
				.attr("data-id", $(this).attr("data-id"));
		})
	})
	.fail(function(e){
		console.log(e);
	})
}

function deleteContact(resource_uri, id){
    $.ajax({
        type: "GET",
        url: resource_uri,
        success: function(contact){
            contact["visible"] = 0;
                $.ajax({
                    type: "PUT",
                    url: resource_uri,
                    contentType: "application/json",
                    data: JSON.stringify(contact),
                    beforeSend: function(jqXHR, settings) {
                        jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                    },
                    success: function(delete_contact){
                        getContact("/api/v1/contact/?contact_type=1");
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
        },
        error: function(message){
            console.log(message);
        }
    });
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getContact(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#contact_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getContact(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#contact_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}