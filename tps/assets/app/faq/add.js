$(function(){
	//$("#faq_btn").text("Agregar nueva FAQ");
	$("#id_title").addClass("tastypie_error");

	// => Función que se encarga de generar una nueva FAQ
	$("#faq_btn").click(function(e){
		$("#faq_btn").text("Cargando datos...");
		e.preventDefault();

		$('#id_answer').tinymce().save();

		var faq_data = $("#faq_form").serializeObject();

		var resource_uri = "/api/v1/faq/";

		var settings = {
		  "async": true,
		  "crossDomain": true,
		  "url": resource_uri,
		  "method": "POST",
		  "headers": {
		    "cache-control": "no-cache",
		    "X-CSRFToken": $('input[name=csrfmiddlewaretoken]').val()
		  },
		  "processData": false,
		  "contentType": "application/json",
		  "data": JSON.stringify(faq_data)
		}

		$.ajax(settings)
			.done(function (response) {
			  	$(".form-validation").remove();
				$("#faq_btn").text("Agregar nueva FAQ");
				$("#alert_ok").show();
				$("#alert_error").hide();

				location.href = '/faq/';

			    // window.setTimeout(function(){
			    //     window.location.href = document.referrer;
			    // }, 1000);
			})
			.fail(function(e){
				console.log(e);
				$(".form-validation").remove();
				$("form :input")
					.css('border', '')
					.css('background-color', '');
				$("#faq_btn").text("Agregar nueva FAQ");
				$("#alert_ok").hide();
				$("#alert_error").show();
				errors = JSON.parse(e["responseText"]);
				$.each(errors["faq"], function(field, error){
					$("#id_"+field)
						.css('border', '1px solid #a94442')
						.css('background-color', '#f2dede')
						.after("<span class='form-validation' style='color:red'>"+error+" </span>");
				})
			})
	});
	
	$('.faq-link').addClass('active-link');
});