$(function(){
	getFaq("/api/v1/faq/");
	$(".page").click(function(){
		getFaq($(this).attr("data-url"));
	})
	$("#btn_confirm_delete").click(function(){
		deleteFaq($(this).attr("data-uri"),$(this).attr("data-id") );
	})
	$('.faq-link').addClass('active-link');
})

function getFaq(resource_uri){
	$.ajax({
		type : "GET",
		url : resource_uri
	})
	.done(function(faqs){
		
		var rows = "";
		$.each(faqs["objects"], function(i, faq){
			var btn_update = "<a href='/faq/"+faq["id"]+"/update/' class='btn btn-lg btn-icon btn-info'><i class='fa fa-edit'></i></a>";
			var btn_delete = "";
			if (faq["id"]>16 && faq["id"]<24) {
				btn_delete = "";
			} else {
				btn_delete = "<a href='#!'  data-toggle='modal' data-id='"+faq["id"]+"' data-uri='"+faq["resource_uri"]+"' data-target='#deleteFaq' class='btn btn-lg btn-icon btn-danger btn-delete-faq'><i class='fa fa-trash'></i></a>";
			}
			var row = "<tr>"+
				"<td>"+faq["question"]+"</td>"+
				"<td><div class='text-ellipsis'>"+faq["answer"]+"</div></td>"+
				"<td>"+btn_update+"&nbsp;"+btn_delete+"</td>"+
				"</tr>";
			rows += row;
		});

		$("#faq_table tbody").html(rows);
		set_pagination_results(faqs);

		$(".btn-delete-faq").click(function(){
			$("#btn_confirm_delete")
				.attr("data-uri", $(this).attr("data-uri"))
				.attr("data-id", $(this).attr("data-id"));
		})
	})
	.fail(function(e){
		console.log(e);
	})
}

function deleteFaq(resource_uri, id){
    $.ajax({
        type: "GET",
        url: resource_uri,
        success: function(faq){
            faq["visible"] = 0;
                $.ajax({
                    type: "PUT",
                    url: resource_uri,
                    contentType: "application/json",
                    data: JSON.stringify(faq),
                    beforeSend: function(jqXHR, settings) {
                        jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                    },
                    success: function(delete_faq){
                        getFaq("/api/v1/faq/");
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
        },
        error: function(message){
            console.log(message);
        }
    });
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getFaq(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#faq_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getFaq(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#faq_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}