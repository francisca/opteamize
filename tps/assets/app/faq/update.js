$(function(){
	getFaq();
	$("#btn_update").click(function(e){
		updateFaq(e);
	})
	$('.faq-link').addClass('active-link');
})

function getFaq(){
	var faq_id = $("#faq_id").val();
	$.ajax({
		type : "GET",
		url : "/api/v1/faq/"+faq_id+"/",
		async: false,
	}).done(function(faq){
		$("#id_question").val(faq["question"]);
		$("#id_answer").val(faq["answer"]);
	}).fail(function(e){
		console.log(e);
	})
}

function updateFaq(e){
	e.preventDefault();
	var faq_id = $("#faq_id").val();
	$('#id_answer').tinymce().save();
	var faq_data = $("form").serializeObject();
	
	console.log(faq_data);

	if ($("#id_type").val() != "") {
		faq_data["type"] = "/api/v1/faq_type/"+$("#id_type").val()+"/";
	}else{
		$("#id_type")
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Este campo es obligatorio</span>");
			$("#faq_btn").text("Agregar nueva FAQ");
		return false;
	}

	$.ajax({
		type : "PUT",
		url : "/api/v1/faq/"+faq_id+"/",
		data : JSON.stringify(faq_data),
		contentType : "application/json",
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
        }
	}).done(function(faq){
		$("#success_faq").show();
		// mostramos mensaje de éxito
	  	$(".form-validation").remove();
		$("#faq_btn").text("Agregar nueva FAQ");
		$("#alert_ok").show();
		$("#alert_error").hide();

	    window.setTimeout(function(){
	        window.location.href = document.referrer;
	    }, 1000);

	}).fail(function(e){
		$(".form-validation").remove();
		$("#success_faq").hide();
		$("form :input")
			.css('border', '')
			.css('background-color', '');
		$("#faq_btn").text("Agregar nueva FAQ");
		$("#alert_ok").hide();
		$("#alert_error").show();
		errors = JSON.parse(e["responseText"]);
		$.each(errors["faq"], function(field, error){
			$("#id_"+field)
				.css('border', '1px solid #a94442')
				.css('background-color', '#f2dede')
				.after("<span class='form-validation' style='color:red'>"+error+" </span>");
		})
	})
}