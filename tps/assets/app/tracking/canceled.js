$(document).on("ready", function(){
	/* Obtener historial de consultas */ 
	resource_uri = "/api/v1/travel_list/?finish_type=2";
    filter = "";
    order = "";

	$('input[name="input_filter_date"]').daterangepicker({
          "format": 'YYYY-MM-DD',
          locale:{
            applyLabel: 'Seleccionar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta'
          },
          maxDate: new Date()
        });

	getCanceledTravels("/api/v1/travel_list/?finish_type=2");
	getChartCancelTravels(resource_uri);
	
	$(".page").click(function(){
		getCanceledTravels($(this).attr("data-url"));
	})

	$("#btn_reload").click(function(){
		resource_uri = "/api/v1/travel_list/?finish_type=2";
        filter = "";
        order = "";
		getCanceledTravels(resource_uri);
	});

	$("#btn-search").on('click', searchTravel);

	$("#btn-list").click(function(){
		$(".empty_query").hide('fast');
		$("#id_date").val('');
		resource_uri = "/api/v1/travel_list/?finish_type=2";
        filter = "";
        order = "";
		getCanceledTravels(resource_uri);
	});

	order_by_colummn();

    $('.canceled-link').addClass('active-link');

    $('#open-filter').click(function(e){
        e.preventDefault();
        if($(this).hasClass('btn-filter-active')) {
            $('#box-filter-travel').hide('fast');
            $(this).removeClass('btn-filter-active');
        } else {
            $('#box-filter-travel').show('fast');
            $(this).addClass('btn-filter-active');
        }
    });

});

function order_by_colummn(){
    var boolTag = true;
    $("tr#table_group th").click(function() {
        var order_by = $(this).attr('id');

        if (order_by) {
            if (boolTag == true) {
                $("#"+order_by+"_desc").removeClass("fa-sort-desc");
                $("#"+order_by+"_desc").addClass("fa-sort-asc");
                order = "&order_by=-"+order_by;
                boolTag = false;
            }else{
                $("#"+order_by+"_desc").removeClass("fa-sort-asc");
                $("#"+order_by+"_desc").addClass("fa-sort-desc");               
                order = "&order_by="+order_by;  
                boolTag = true;
            }
            $('.box-gif').css('display','block');
            $("#canceled_table").css('display','none');
            getCanceledTravels(resource_uri+filter+order);
        }
    }); 
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function getCanceledTravels(resource_uri){
	console.log(resource_uri);
    document.getElementById("btn_export").href = "export_canceled/?url="+filter.replaceAll("&","?")+order.replaceAll("&","?");
	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){
            
			var rows = "";
			$.each(data["objects"], function(i, canceled){
				containers = canceled["container_is_carried"];
				var tracking_numbers = "";
				$.each(containers, function(i, container){
					tracking_numbers += (tracking_numbers.length > 0 ? ', ' : '') + container["tracking_number"];
				});
                
                duration = daysHoursMin(canceled["duration"]);

				var btn_detail = "<a href='/tracking/"+canceled["id"]+"/canceled_detail/' class='btn btn-info btn-lg btn-icon'><i class='fa fa-eye'></i></a>";
				var row = "<tr>"+
					"<td>"+canceled["user"]["rut"]+"</td>"+
					"<td>"+canceled["user"]["last_name"]+", "+canceled["user"]["first_name"]+"</td>"+
					"<td>"+canceled["user"]["contact_phone"]+"</td>"+
					"<td>"+tracking_numbers+"</td>"+
					"<td>"+format_datetime(canceled["finish_date"])+"</td>"+
					"<td>"+duration+"</td>"+
					"<td>"+btn_detail+"</td>"+
					"</tr>";
				
				rows += row;	
			});

            $('.box-gif').css('display','none');
            $("#canceled_table").css('display','table');
			$("#canceled_table tbody").html(rows);
            set_pagination_results(data);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function searchTravel(){
    var query = false;
    resource_uri = "/api/v1/travel_list/?finish_type=2";
    filter = "";
    order = "";
    var id_date = $('#id_date').val();

    if (id_date != "") {
        query = true;
    }

    if (query == true) {
        range_date = id_date.split(" - ");
        filter += "&finish_date__range="+range_date[0]+" 00:00:00,"+range_date[1]+" 23:59:59";
        query = true;
    }
    console.log(resource_uri);
    if (!query) {
        $(".empty_query").show('fast');
    } else {
        $('.empty_query').hide('fast');
        getCanceledTravels(resource_uri+filter);
        $("html, body").delay(100).animate({scrollTop: $('#box_travel_table').offset().top }, 800);
    }

}

function getChartCancelTravels(resource_uri){
	var info_list = "";
	var labels = [];
    var data_info = [];
    var data_total = [];

	for (i = 0; i < 7; i++) {
	    var date = new Date();
    	date.setDate(date.getDate()-i);
    	pass_date = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate();
    	filter = "&limit=0&finish_date__range="+pass_date+" 00:00:00,"+pass_date+" 23:59:59";
    	filter_total = "/api/v1/travel_list/?finish_type=1&limit=0&finish_date__range="+pass_date+" 00:00:00,"+pass_date+" 23:59:59";
    	$.ajax({
    		type: "GET",
    		async: false,
    		url: filter_total,
    		success: function(data){
    			data_total[i] = data["meta"]["total_count"];
    		},
    		error: function(e){
    			console.log(e);
    		}
    	})
    	$.ajax({
    		type : "GET",
    		async: false,
			url : resource_uri+filter,
			success: function(data){
				data_info[i] = data["meta"]["total_count"];
			},
			error: function(e){
				console.log(e);
			}
	    });

    	info_row = "<tr>"+
	        "<td>"+ pass_date +"</td>"+
	        "<td>"+ data_info[i] +"</td>"+
	        "<td>"+ data_total[i] + "</td>";
	        "</tr>";
	    info_list += info_row;

    	labels.push(pass_date);
	}

	$("#cancelChart_table tbody").html(info_list);

	var cdata = {
                labels: labels,
                datasets: [
                    {
                        data: data_info,
                        backgroundColor: "#fe8b8b"
                    },
                    {
                    	data: data_total,
                    	backgroundColor: "#74bfff"
                    }
                    ]
            };

    var ctx = document.getElementById("cancelChart").getContext('2d');
    var myDoughnutChart = new Chart(ctx, {
        type: 'bar',
        data: cdata,
        options: {
        	responsive: true,
        	legend: {
	            display: false
	        },
	        animation: {
                animateScale: true
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function (value) { if (Number.isInteger(value)) { return value; } },
                        stepSize: 1
                    }
                }]
            }
	    }
    });
}

function set_pagination_results(meta_objet){
    var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
    var resource_uri = "";
    if (total_pages == 0) {
        $('#page-selection').hide();
    }else{
        $('#page-selection').show();
        $('#page-selection').bootpag({
            total: total_pages,
            maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
        });
        $('ul.pagination.bootpag li').click(function() {
            var num = $(this).attr('data-lp');
            offset = parseInt(num - 1) * 20;

            if (meta_objet["meta"]["next"]) {
                resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);   
                getCanceledTravels(resource_uri);
                $("html, body").delay(50).animate({scrollTop: $('#cancelChart_table').offset().top }, 800);
            }

            if(meta_objet["meta"]["previous"]) {
                resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
                getCanceledTravels(resource_uri);   
                $("html, body").delay(50).animate({scrollTop: $('#cancelChart_table').offset().top }, 800);
            }
        });
    }
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}