$(function(){
	loadReport();
    /*$(".page").click(function(){
        getUserLocation($(this).attr("data-url"));
        $("html, body").delay(100).animate({scrollTop: $('#travel_table').offset().top }, 1200);
    })*/
	$(".back_query").attr('href', '/tracking/query/');
	$('.query-link').addClass('active-link');
});

function initMap(coordinatesTravel) {

    console.log(coordinatesTravel)

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: {lat: coordinatesTravel[0]["lat"], lng: coordinatesTravel[0]["lng"]}
    });   

    var flightPath = new google.maps.Marker({
      position: coordinatesTravel[0],
      geodesic: true,
      strokeColor: '#FF0000',
      strokeOpacity: 1.0,
      strokeWeight: 2
    });

    flightPath.setMap(map);
  }

function secondsToHms(d) {
    d = Number(d);

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

function loadReport(){
	var host = window.location.host;
	var query_id = $("#query_id").val();
	$.ajax({
		type : "GET",
		url : "/api/v1/tracking_query/"+query_id+"/",
		success: function(query){

			user_id = query["user"]["id"];

            /*getUserLocation("/api/v1/user_location_list/?limit=0&user_id="+user_id+"&travel="+query_id, 0)
            
            getUserLocation("/api/v1/user_location_list/?limit=8&user_id="+user_id+"&travel="+query_id, 1)*/

			$("#id_user_name").text(query["user"]["last_name"]+", "+query["user"]["first_name"]);
			$("#id_user_rut").text(query["user"]["rut"]);
			$("#id_user_phone").text(query["user"]["contact_phone"]);
			$("#id_user_mail").text(query["user"]["email"]);

			container = query["container"];
            var tracking_numbers = "";
            tracking_numbers += (tracking_numbers.length > 0 ? ', ' : '') + container["tracking_number"];

            var fromTime = new Date(query["timestamp"]);
            
            var path = [];
            coordinates = {}
            coordinates["lat"] = parseFloat(query["latitude"]);
            coordinates["lng"] = parseFloat(query["longitude"]);
            path.push(coordinates);
            initMap(path);
            $('.box-gif').css('display','none');
            $('#map').css('display', 'block');

			$("#id_route_from").text(format_datetime(query["timestamp"]));
			$("#id_route_container").text(tracking_numbers);
			
		},
		error: function(e){
			console.log(e);
		}
	});
}

/*function getUserLocation(resource_uri, mode){
    $.ajax({
        type : "GET",
        url : resource_uri,
        success: function(data){
            
            var rows = "";
            var path = [];
            $.each(data["objects"], function(i, user_location){
                
                var _initial = user_location["timestamp"];
                var timestamp = new Date(_initial);

                if (mode==0) {
                    coordinates = {}
                    coordinates["lat"] = user_location["latitude"];
                    coordinates["lng"] = user_location["longitude"];
                    path.push(coordinates);
                } else {
                    if (user_location["travel_step"] == 1) {
                        travel_step = "Camino a ZEAL";
                    } else if (user_location["travel_step"] == 2) {
                        travel_step = "En ZEAL";
                    } else if (user_location["travel_step"] == 3) {
                        travel_step = "Camino a TPS";
                    } else if (user_location["travel_step"] == 4) {
                        travel_step = "En TPS";
                    }

                    var row = "<tr>"+
                        "<td>"+user_location["latitude"]+", "+user_location["longitude"]+"</td>"+
                        "<td>"+timestamp.toLocaleString()+"</td>"+
                        "<td>"+(user_location["distance"]/1000).toFixed(1)+" KM </td>"+
                        "<td>"+secondsToHms(user_location["eta"])+"</td>"+
                        "<td>"+travel_step+"</td>"+
                        "</tr>";
                    
                    rows += row;
                }
            });

            if(mode == 0) {
                initMap(path);
                $('.box-gif').css('display','none');
                $('#map').css('display', 'block');
            } else {
                var next_page = data["meta"]["next"];
                var prev_page = data["meta"]["previous"];
                if (!!next_page){
                    // Hay página siguiente
                    $("#next-page").attr("data-url", data["meta"]["next"]);
                    $("#next-page").show();
                }
                else{
                    // NO hay página siguiente
                    $("#next-page").hide();
                }

                if (!!prev_page){
                    // Hay página previa
                    $("#prev-page").attr("data-url", data["meta"]["previous"]);
                    $("#prev-page").show();
                }
                else{
                    // NO Hay página siguiente  
                    $("#prev-page").hide();
                }

                $("#travel_table tbody").html(rows);
            }
            
        },
        error: function(e){
            console.log(e);
        }
    });
}*/