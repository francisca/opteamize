$(function(){
	/* Obtener listado relaciones container_contact_user */ 
	
	resource_uri = "/api/v1/container_contact_user_list/";
	filter = "";
	order = "";

	getQueries("/api/v1/container_contact_user_list/");

	$(".page").click(function(){
		$('.box-gif').css('display','block');
		$("#travel_table").css('display','none');
		getQueries($(this).attr("data-url"));
	})

	$("#btn_reload").click(function(){
		resource_uri = "/api/v1/container_contact_user_list/";
		filter = "";
		order = "";
		$('.box-gif').css('display','block');
		$("#travel_table").css('display','none');
		getQueries("/api/v1/container_contact_user_list/");
	});

	$("#btn-search").on('click', searchContainer);

	$("#btn-list").click(function(){
		$(".empty_query").hide('fast');
		$("#id_date").val('');
		$("#id_arrival_type").val(0);
		resource_uri = "/api/v1/container_contact_user_list/";
		filter = "";
		order = "";	
		$('.box-gif').css('display','block');
		$("#travel_table").css('display','none');
		getQueries("/api/v1/container_contact_user_list/");
		$("html, body").delay(100).animate({scrollTop: $('#box_travel_table').offset().top }, 800);
	});

	$('#open-filter').click(function(e){
		e.preventDefault();
		if($(this).hasClass('btn-filter-active')) {
			$('#box-filter-travel').hide('fast');
			$(this).removeClass('btn-filter-active');
		} else {
			$('#box-filter-travel').show('fast');
			$(this).addClass('btn-filter-active');
		}
	});

	order_by_colummn();

	$('.contact_container-link').addClass('active-link');
});

/*ordenamiento por RUT, 
				nombre, 
				inicio viaje, 
				eta y 
				tramo.*/

function order_by_colummn(){
	var boolTag = true;
	$("tr#table_group th").click(function() {
		var order_by = $(this).attr('id');

		if (order_by) {
		    if (boolTag == true) {
		    	$("#"+order_by+"_desc").removeClass("fa-sort-desc");
		    	$("#"+order_by+"_desc").addClass("fa-sort-asc");

		    	boolTag = false;
		    	if (order_by == "reception_date") {
		    		if(filter.indexOf("?") !== -1){
			    		order = "&order_by=container__reception_date&order_by=-container__is_active";
			    	}else{
			    		order = "?order_by=container__reception_date&order_by=-container__is_active";
			    	}
		    	} else if (order_by == "stacking_finish") {
		    		
		    		if(filter.indexOf("?") !== -1){
			    		order = "&order_by=container__stacking_finish_date";
			    	}else{
			    		order = "?order_by=container__stacking_finish_date";
			    	}
		    	} else {
		    		if(filter.indexOf("?") !== -1){
			    		order = "&order_by=-"+order_by;
			    	}else{
			    		order = "?order_by=-"+order_by;
			    	}
		    	}
		    }else{
		    	$("#"+order_by+"_desc").removeClass("fa-sort-asc");
		    	$("#"+order_by+"_desc").addClass("fa-sort-desc");
		    	
		    	boolTag = true;
		    	if (order_by == "reception_date") {
		    		if(filter.indexOf("?") !== -1){
			    		order = "&order_by=-container__is_active&order_by=-container__reception_date";
			    	}else{
			    		order = "?order_by=-container__is_active&order_by=-container__reception_date";
			    	}
		    	} else if (order_by == "stacking_finish") {
		    		if(filter.indexOf("?") !== -1){
			    		order = "&order_by=-container__stacking_finish_date";
			    	}else{
			    		order = "?order_by=-container__stacking_finish_date";
			    	}
		    	} else {
		    		if(filter.indexOf("?") !== -1){
			    		order = "&order_by="+order_by;
			    	}else{
			    		order = "?order_by="+order_by;
			    	}
		    	}
		    }
		    $('.box-gif').css('display','block');
		    $("#travel_table").css('display','none');
			getQueries(resource_uri+filter+order);
		}
	});	
}


String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function searchContainer(){
    var query = false;
    resource_uri = "/api/v1/container_contact_user_list/";
    filter = "";
    order = "";
    var id_arrival_type = $('#id_arrival_type').val();

    if (id_arrival_type != 0) {
    	filter += "?container__arrival_type="+id_arrival_type;
        query = true;
    }

    if (!query) {
        $(".empty_query").show('fast');
    } else {
        $('.empty_query').hide('fast');
        console.log(resource_uri+filter);
        $('.box-gif').css('display','block');
		$("#travel_table").css('display','none');
        getQueries(resource_uri+filter);
        $("html, body").delay(100).animate({scrollTop: $('#box_travel_table').offset().top }, 800);
    }

}

function getQueries(resource_uri){
	console.log(resource_uri);
	document.getElementById("btn_export").href = "export_contact/?url="+filter.replaceAll("&","?")+order.replaceAll("&","?");
	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){
			
			var rows = "";
			$.each(data["objects"], function(i, contact_container){
				var stacking_flag = true;
				var reception_flag = true;
				if (contact_container["container"]["stacking_finish_date"] == "") {
					finish_stacking = "Sin información";
					stacking_flag = false;
				} else {
					finish_stacking = format_datetime_basic(contact_container["container"]["stacking_finish_date"]);
				}

				if (contact_container["container"]["reception_date"] == ""){
					reception_date = "-";
					reception_flag = false;
				} else {
					reception_date = format_datetime_basic(contact_container["container"]["reception_date"]);
				}

				if (stacking_flag && reception_flag) {
					if(contact_container["container"]["reception_date"] > contact_container["container"]["stacking_finish_date"]){
						reception_date = reception_date+"<i class='fa fa-exclamation-circle' style='margin-left: 6px;color: #dc0303;'></i><p class='text-late'>LATE</p>";
					} else {
						reception_date = reception_date+"<i class='fa fa-clock-o' style='margin-left: 6px;color: #7bce1a;'></i><p class='text-in-hour'>EN HORA</p>";
					}
				}
				var row = "<tr>"+
					"<td>"+contact_container["user"]["rut"]+"</td>"+
					"<td>"+contact_container["user"]["last_name"]+", "+contact_container["user"]["first_name"]+"</td>"+
					"<td>"+contact_container["contact"]["name"]+"</td>"+
					"<td>"+contact_container["container"]["tracking_number"]+"</td>"+
					"<td>"+finish_stacking+"</td>"+
					"<td>"+reception_date+"</td>"+
					"</tr>";
				
				rows += row;	
			});

			$('.box-gif').css('display','none');
		    $("#travel_table").css('display','table');
			$("#contact_container_table tbody").html(rows);
			set_pagination_results(data);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getQueries(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#travel_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getQueries(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#travel_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}