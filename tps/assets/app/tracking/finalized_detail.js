$(function(){
	loadReport();
    $(".page").click(function(){
        getUserLocation($(this).attr("data-url"));
        $("html, body").delay(100).animate({scrollTop: $('#travel_table').offset().top }, 1200);
    })
	$(".back_travel").attr('href', '/tracking/finalized/');
	
    $('.finalized-link').addClass('active-link');
});

function initMap(coordinatesTravel) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: {lat: coordinatesTravel[0]["lat"], lng: coordinatesTravel[0]["lng"]}
    });   

    if(coordinatesTravel.length>1){
        var flightPath = new google.maps.Polyline({
          path: coordinatesTravel,
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        flightPath.setMap(map);
    }else{
        var flightPath = new google.maps.Marker({
          position: coordinatesTravel[0],
          geodesic: true,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2
        });

        flightPath.setMap(map);
    }
  }

function secondsToHms(d) {
    d = Number(d);

    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

function loadReport(){
	var host = window.location.host;
	var travel_id = $("#travel_id").val();
	$.ajax({
		type : "GET",
		url : "/api/v1/travel_list/"+travel_id+"/",
		success: function(travel){

			user_id = travel["user"]["id"];

            getUserLocation("/api/v1/user_location_list/?limit=0&user_id="+user_id+"&travel="+travel_id, 0)
            
            getUserLocation("/api/v1/user_location_list/?limit=8&user_id="+user_id+"&travel="+travel_id, 1)

			$("#id_user_name").text(travel["user"]["last_name"]+", "+travel["user"]["first_name"]);
			$("#id_user_rut").text(travel["user"]["rut"]);
			$("#id_user_phone").text(travel["user"]["contact_phone"]);
			$("#id_user_mail").text(travel["user"]["email"]);

			containers = travel["container_is_carried"];
            var tracking_numbers = "";
            $.each(containers, function(i, container){
                tracking_numbers += (tracking_numbers.length > 0 ? ', ' : '') + container["tracking_number"];
            });

            var _initial = travel["init_date"];
            var _final = travel["finish_date"];
            var fromTime = new Date(_initial);
            var toTime = new Date(_final);

            var differenceTravel = toTime.getTime() - fromTime.getTime();
            var seconds = Math.floor((differenceTravel) / (1000));

            var time =  daysHoursMin(seconds);

			$("#id_route_from").text(travel["init_place"]);
            $("#id_date_init").text(format_datetime(travel["init_date"]));
            $("#id_date_finish").text(format_datetime(travel["finish_date"]));
			$("#id_route_distance").text((travel["distance"]/1000).toFixed(1) + " km");
			$("#id_route_duration").text(time);
			$("#id_route_container").text(tracking_numbers);
			

		},
		error: function(e){
			console.log(e);
		}
	});
}

function getUserLocation(resource_uri, mode){
    $.ajax({
        type : "GET",
        url : resource_uri,
        success: function(data){
            /* ===== Paginación ===*/
            
            /* === Fin paginación ==  */

            var rows = "";
            var path = [];
            $.each(data["objects"], function(i, user_location){
                
                var _initial = user_location["timestamp"];
                var timestamp = new Date(_initial);

                if (mode==0) {
                    coordinates = {}
                    coordinates["lat"] = user_location["latitude"];
                    coordinates["lng"] = user_location["longitude"];
                    path.push(coordinates);
                } else {
                    if (user_location["travel_step"] == 1) {
                        travel_step = "Camino a ZEAL";
                    } else if (user_location["travel_step"] == 2) {
                        travel_step = "En ZEAL";
                    } else if (user_location["travel_step"] == 3) {
                        travel_step = "Camino a TPS";
                    } else if (user_location["travel_step"] == 4) {
                        travel_step = "En TPS";
                    }

                    var row = "<tr>"+
                        "<td>"+user_location["latitude"]+", "+user_location["longitude"]+"</td>"+
                        "<td>"+format_datetime(user_location["timestamp"])+"</td>"+
                        "<td>"+(user_location["distance"]/1000).toFixed(1)+" km </td>"+
                        "<td>"+secondsToHms(user_location["eta"])+"</td>"+
                        "<td>"+travel_step+"</td>"+
                        "</tr>";
                    
                    rows += row;
                }
            });

            if(mode == 0) {
                initMap(path);
                $('.box-gif').css('display','none');
                $('#map').css('display', 'block');
            } else {
                $("#travel_table tbody").html(rows);
                set_pagination_results(data)
            }
            
        },
        error: function(e){
            console.log(e);
        }
    });
}

function set_pagination_results(meta_objet){
    var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 8);
    var resource_uri = "";
    if (total_pages == 0) {
        $('#page-selection').hide();
    }else{
        $('#page-selection').show();
        $('#page-selection').bootpag({
            total: total_pages
        });
        $('ul.pagination.bootpag li').click(function() {
            var num = $(this).attr('data-lp');
            offset = parseInt(num - 1) * 8;

            if (meta_objet["meta"]["next"]) {
                resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);   
                getUserLocation(resource_uri);
                $("html, body").delay(100).animate({scrollTop: $('#travel_table').offset().top }, 800);
            }

            if(meta_objet["meta"]["previous"]) {
                resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
                getUserLocation(resource_uri);
                $("html, body").delay(100).animate({scrollTop: $('#travel_table').offset().top }, 800);
            }
        });
    }
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}