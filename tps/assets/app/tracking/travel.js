var map = "";
var markers = [];
function initMap() {

    var myLatLng = {lat: -33.0696375, lng: -71.637945};

    map = new google.maps.Map(document.getElementById('map'), {
      	zoom: 10,
      	//center: {lat: coordinatesTravel[0]["lat"], lng: coordinatesTravel[0]["lng"]}
    	center: myLatLng
    });   

    $('.box-gif').css('display','none');
    $('#map').css('display', 'block');
  }

$(function(){

	resource_uri = "/api/v1/user_location/";
	limit0 = "limit=0";
	filter = "";
	order = "";

	$('input[name="input_filter_date"]').daterangepicker({
          "format": 'YYYY-MM-DD',
          locale:{
            applyLabel: 'Seleccionar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta'
          },
          maxDate: new Date()
        });
	/* Obtener listado de viajes en curso */ 
	
	getTravels(resource_uri);
	getAllTravels(resource_uri+"?"+limit0);

	$("#btn_reload").click(function(){
		resource_uri = "/api/v1/user_location/";
		filter = "";
		order = "";
		getTravels(resource_uri);
	});

	$("#btn-search").on('click', searchTravel);

	$("#btn-list").click(function(){
		$(".empty_query").hide('fast');
		$("#id_date").val('');
		$("#id_travel_step").val(0);
		resource_uri = "/api/v1/user_location/";
		filter = "";
		order = "";
		getTravels(resource_uri);
		$("html, body").delay(100).animate({scrollTop: $('#box_travel_table').offset().top }, 800);
	});

	order_by_colummn();

	$('.travel-link').addClass('active-link');

	$('#open-filter').click(function(e){
		e.preventDefault();
		if($(this).hasClass('btn-filter-active')) {
			$('#box-filter-travel').hide('fast');
			$(this).removeClass('btn-filter-active');
		} else {
			$('#box-filter-travel').show('fast');
			$(this).addClass('btn-filter-active');
		}
	});

});

/*ordenamiento por RUT, 
				nombre, 
				inicio viaje, 
				eta y 
				tramo.*/

function order_by_colummn(){
	var boolTag = true;
	$("tr#table_group th").click(function() {
		var order_by = $(this).attr('id');

		if (order_by) {
		    if (boolTag == true) {
		    	$("#"+order_by+"_desc").removeClass("fa-sort-desc");
		    	$("#"+order_by+"_desc").addClass("fa-sort-asc");
		    	if(filter.indexOf("?") !== -1){
		    		order = "&order_by=-"+order_by;
		    	}else{
		    		order = "?order_by=-"+order_by;
		    	}
		    	
		    	boolTag = false;
		    }else{
		    	$("#"+order_by+"_desc").removeClass("fa-sort-asc");
		    	$("#"+order_by+"_desc").addClass("fa-sort-desc");		    	
		    	if(filter.indexOf("?") !== -1){
		    		order = "&order_by="+order_by;	
		    	}else{
		    		order = "?order_by="+order_by;	
		    	}
				boolTag = true;
		    }
		    $('.box-gif').css('display','block');
		    $("#travel_table").css('display','none');
		    
			getTravels(resource_uri+filter+order);
		}
	});	
}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function getTravels(resource_uri){
	document.getElementById("btn_export").href = "export_travel/?url="+filter.replaceAll("&","?")+order.replaceAll("&","?");

	setMapOnAll(null);
	markers = [];
	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){
			var rows = "";
			$.each(data["objects"], function(i, travel){
				var image = {
				    url: '/media/pin.png',
				    // This marker is 20 pixels wide by 32 pixels high.
				    size: new google.maps.Size(40, 40),
				    // The origin for this image is (0, 0).
				    origin: new google.maps.Point(0, 0),
				    // The anchor for this image is the base of the flagpole at (0, 32).
				    anchor: new google.maps.Point(0, 40),
				    scaledSize: new google.maps.Size(30, 38)
				};
				var marker = new google.maps.Marker({
					position: {lat: travel["latitude"], lng: travel["longitude"]},
					geodesic: true,
					strokeColor: '#FF0000',
					strokeOpacity: 1.0,
					strokeWeight: 2,
					icon: image,
					title: 'Viaje en curso'
			    });

			    markers.push(marker);

				containers = travel["travel"]["container_is_carried"];
				var tracking_numbers = "";
				$.each(containers, function(i, container){
					tracking_numbers += (tracking_numbers.length > 0 ? ', ' : '') + container["tracking_number"];
				});
				if(travel["travel_step"] == 1) {
					travel_step = "Camino a ZEAL";
					eta = daysHoursMin(travel["eta"]);
					// minutes = travel["eta"] / 60;
					// minutes = parseInt(minutes);
					// if (minutes > 60) {
					// 	hours = minutes / 60;
					// 	hour_minutes = minutes - (parseInt(hours) * 60);
					// 	total = parseInt(hours) + "h " + hour_minutes + "min"; 
					// } else {
					// 	total = minutes + " min"
					// }
				} else {
					eta = "-";
					if (travel["travel_step"] == 2) {
						travel_step = "En ZEAL";
					} else if (travel["travel_step"] == 3) {
						travel_step = "Camino a TPS";
					} else if (travel["travel_step"] == 4) {
						travel_step = "En TPS";
					} else {
						travel_step = "Camino a ZEAL";
					}
				}
				var btn_detail = "<a href='/tracking/"+travel["travel"]["id"]+"/detail/' class='btn btn-info btn-lg btn-icon'><i class='fa fa-eye'></i></a>";
				var row = "<tr>"+
					"<td>"+travel["user"]["rut"]+"</td>"+
					"<td>"+travel["user"]["last_name"]+", "+travel["user"]["first_name"]+"</td>"+
					"<td>"+travel["user"]["contact_phone"]+"</td>"+
					"<td>"+tracking_numbers+"</td>"+
					"<td>"+format_datetime(travel["travel"]["init_date"])+"</td>"+
					"<td>"+eta+ "</td>"+
					"<td>"+travel_step+"</td>"+
					"<td>"+btn_detail+"</td>"+
					"</tr>";
				
				rows += row;	
			});
			$('.box-gif').css('display','none');
			setMapOnAll(map);
			$("#travel_table").css('display','table');
			$("#travel_table tbody").html(rows);
			set_pagination_results(data);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function getAllTravels(resource_uri){
	setMapOnAll(null);
	markers = [];
	console.log(resource_uri);
	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){
			var rows = "";
			$.each(data["objects"], function(i, travel){
				var image = {
				    url: '/media/pin.png',
				    // This marker is 20 pixels wide by 32 pixels high.
				    size: new google.maps.Size(40, 40),
				    // The origin for this image is (0, 0).
				    origin: new google.maps.Point(0, 0),
				    // The anchor for this image is the base of the flagpole at (0, 32).
				    anchor: new google.maps.Point(0, 40),
				    scaledSize: new google.maps.Size(30, 38)
				};
				var marker = new google.maps.Marker({
					position: {lat: travel["latitude"], lng: travel["longitude"]},
					geodesic: true,
					strokeColor: '#FF0000',
					strokeOpacity: 1.0,
					strokeWeight: 2,
					icon: image,
					title: 'Viaje en curso'
			    });

			    markers.push(marker);	
			});
			$('.box-gif').css('display','none');
			setMapOnAll(map);
		},
		error: function(e){
			console.log(e);
		}
	});
}

function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

function searchTravel(){
	var query = false;
	resource_uri = "/api/v1/user_location/";
	filter = "";
	order = "";
	var id_travel_step = $("#id_travel_step").val();
	var id_date = $('#id_date').val();

	if (id_travel_step != 0) {
		filter += "?travel_step="+id_travel_step;
		query = true;
	}

	if (id_date != "") {
		range_date = id_date.split(" - ");

		if(filter.indexOf("?") !== -1){
    		filter += "&travel__init_date__range="+range_date[0]+" 00:00:00,"+range_date[1]+" 23:59:59";
    	}else{
    		filter += "?travel__init_date__range="+range_date[0]+" 00:00:00,"+range_date[1]+" 23:59:59";
    	}
		query = true;
	}

	if (!query) {
		$(".empty_query").show('fast');
	} else {
		$('.empty_query').hide('fast');
		getTravels(resource_uri+filter);
		getAllTravels(resource_uri+filter+"&limit=0")
		$("html, body").delay(100).animate({scrollTop: $('#box_travel_table').offset().top }, 800);
	}

}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getTravels(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#travel_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getTravels(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#travel_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}