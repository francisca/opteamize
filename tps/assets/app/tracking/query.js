var empty_query = false;

$(function(){
	/* Obtener historial de consultas */
	$("html, body").delay(100).animate({scrollTop: $('#box_query_chart').offset().top }, 400);
	
	resource_uri = "/api/v1/tracking_query/";
    filter = "";
    order = "";

	$('input[name="input_filter_date"]').daterangepicker({
          "format": 'YYYY-MM-DD',
          locale:{
            applyLabel: 'Seleccionar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'Hasta'
          },
          maxDate: new Date()
        });

	queries = [];
	
	getQueries(resource_uri);

	$(".page").click(function(){
		getQueries($(this).attr("data-url"));
	})

	$("#btn_reload").click(function(){
		resource_uri = "/api/v1/tracking_query/";
	    filter = "";
	    order = "";
		getQueries(resource_uri);
	});

	$("#btn-search").on('click', searchTravel);

	$("#btn-list").click(function(){
		$(".empty_query").hide('fast');
		$("#id_date").val('');
		resource_uri = "/api/v1/tracking_query/";
	    filter = "";
	    order = "";
		getQueries(resource_uri);
	});

	order_by_colummn();

	$('.query-link').addClass('active-link');

	$('#open-filter').click(function(e){
		e.preventDefault();
		if($(this).hasClass('btn-filter-active')) {
			$('#box-filter-travel').hide('fast');
			$(this).removeClass('btn-filter-active');
		} else {
			$('#box-filter-travel').show('fast');
			$(this).addClass('btn-filter-active');
			$("html, body").delay(100).animate({scrollTop: $('#box-filter-travel').offset().top }, 800);
		}
	});

});

function order_by_colummn(){
	var boolTag = true;
	$("tr#table_group th").click(function() {
		var order_by = $(this).attr('id');

		if (order_by) {
		    if (boolTag == true) {
		    	$("#"+order_by+"_desc").removeClass("fa-sort-desc");
		    	$("#"+order_by+"_desc").addClass("fa-sort-asc");
		    	if(filter.indexOf("?") !== -1){
		    		order = "&order_by=-"+order_by;
		    	}else{
		    		order = "?order_by=-"+order_by;
		    	}
		    	
		    	boolTag = false;
		    }else{
		    	$("#"+order_by+"_desc").removeClass("fa-sort-asc");
		    	$("#"+order_by+"_desc").addClass("fa-sort-desc");		    	
		    	if(filter.indexOf("?") !== -1){
		    		order = "&order_by="+order_by;	
		    	}else{
		    		order = "?order_by="+order_by;	
		    	}
				boolTag = true;
		    }
		    $('.box-gif').css('display','block');
            $("#query_table").css('display','none');
			getQueries(resource_uri+filter+order);
		}
	});	
}

function searchTravel(){
    var query = false;
    resource_uri = "/api/v1/tracking_query/";
    filter = "";
    order = "";
    var id_date = $('#id_date').val();

    if (id_date != "") {
        query = true;
    }

    if (query == true) {
        range_date = id_date.split(" - ");
        filter += "?timestamp__range="+range_date[0]+" 00:00:00,"+range_date[1]+" 23:59:59";
        query = true;
    }

    if (!query) {
        $(".empty_query").show('fast');
    } else {
        $('.empty_query').hide('fast');
        getQueries(resource_uri+filter);
        $("html, body").delay(100).animate({scrollTop: $('#box_travel_table').offset().top }, 800);
    }

}

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function getQueries(resource_uri){
	console.log(resource_uri);
    document.getElementById("btn_export").href = "export_query/?url="+filter.replaceAll("&","?")+order.replaceAll("&","?");

	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){
			
			var rows = "";
			$.each(data["objects"], function(i, query){

				// containers = travel["travel"]["container_is_carried"];
				var region = "";
				if (query["region"] == null) {
					region = "Sin especificar";
				} else {
					region = query["region"]["real_name"];
				}

				var btn_detail = "<a href='/tracking/"+query["id"]+"/query_detail/' class='btn btn-info btn-lg btn-icon'><i class='fa fa-eye'></i></a>";
				var row = "<tr>"+
					"<td>"+query["user"]["rut"]+"</td>"+
					"<td>"+query["user"]["last_name"]+", "+query["user"]["first_name"]+"</td>"+
					"<td>"+query["user"]["contact_phone"]+"</td>"+
					"<td>"+query["container"]["tracking_number"]+"</td>"+
					"<td>"+format_datetime(query["timestamp"])+"</td>"+
					"<td>"+region+"</td>"+
					"<td>"+btn_detail+"</td>"+
					"</tr>";
				
				rows += row;	
			});

			$('.box-gif').css('display','none');
            $("#query_table").css('display','table');
			$("#query_table tbody").html(rows);
			set_pagination_results(data);

			$('.chart-box-gif').css('display','block');

			if(resource_uri.indexOf("?") !== -1){
	    		getChartQueries(resource_uri+"&limit=0");
	    	}else{
	    		getChartQueries(resource_uri+"?limit=0");
	    	}

		},
		error: function(e){
			console.log(e);
		}
	});
}

function getChartQueries(resource_uri){
	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){
			queries = [];
			var rows = "";
			if (data["meta"]["total_count"] == 0) {
				$('.no-register-graph').css('display', 'block');
				$('#regionChart_table').css('display', 'none');
			} else {
				$.each(data["objects"], function(i, query){
					queries.push(query);
				});
				getRegions("/api/v1/region/");
			}

		},
		error: function(e){
			console.log(e);
		}
	});
}

function getRegions(resource_uri){

	var info_list = "";
	var labels = [];
    var data_info = [];
    var color_list = [
        "#ffccbc",
        "#4fc3f7",
        "#ffe57f",
        "#f4ff81",
        "#cddc39",
        "#81c784",
        "#b9f6ca",
        "#f57c00",
        "#9575cd",
        "#f48fb1",
        "#ab47bc",
        "#1e88e5",
        "#22aee4",
        "#e16924",
        "#dbafbf",
        "#e0b71a",
        "#079702"
    ];

	$.ajax({
		type : "GET",
		url : resource_uri,
		success: function(data){

			var rows = "";
			$.each(data["objects"], function(i, query){
				
			    labels.push(query["real_name"]);
			    data_info.push(0);
			});

			labels.push("Sin información");
			var noInfo = 0;

			queries.forEach( function (query){
				var info = false;
				for(var k in labels) {
					if(query["region"]!=null){
						if(query["region"]["real_name"]==labels[k]){
							info = true;
							data_info[k]=data_info[k]+1;
						}
					}
	            }
	            if(info==false){
						noInfo++;	
				}
            });

			data_info.push(noInfo);

	        for(var k in labels) {
	            info_row = "<tr>"+
	                "<td width='20px' style='background-color:"+color_list[k]+"'></td>"+
	                "<td>"+ labels[k] +"</td>"+
	                "<td>"+ data_info[k] +"</td>"+
	                "</tr>";
	            info_list += info_row;
	        }

	        $("#regionChart_table tbody").html(info_list);

	        var cdata = {
	                    labels: labels,
	                    datasets: [
	                        {
	                            data: data_info,
	                            backgroundColor: color_list
	                        }]
	                };

	        $('.chart-box-gif').css('display','none');

	        var ctx = document.getElementById("regionChart").getContext('2d');
	        var myDoughnutChart = new Chart(ctx, {
	            type: 'bar',
	            data: cdata,
	            options: {
	            	legend: {
			            display: false
			        }
			    }
	        });

		},
		error: function(e){
			console.log(e);
		}
	});
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getQueries(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#query_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getQueries(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#query_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}