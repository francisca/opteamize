$(function(){

    $("#btn_update").click(function(){
        updateAdministrator();
    })
    $('.admin-link').addClass('active-link');
})

function updateAdministrator(){
    var validation = true;
    var admin_data = $("form").serializeObject();

    $(".form-validation").remove();

    if(admin_data["password"] != admin_data["password_2"]) {

            validation = false;

            if(admin_data["password"].length > 0){
                $("#password")
                    .css('border', '1px solid #a94442')
                    .css('background-color', '#f2dede')
                    .after("<span class='form-validation' style='color:red'>Las contraseñas no coinciden, por favor corrija e intente nuevamente</span>");
            }
            if(admin_data["password_2"].length > 0){
                $("#password_2")
                .css('border', '1px solid #a94442')
                .css('background-color', '#f2dede')
                .after("<span class='form-validation' style='color:red'>Las contraseñas no coinciden, por favor corrija e intente nuevamente</span>");
            }
    }

    if(admin_data["password"].length == 0){
        validation = false;
        $("#password")
        .css('border', '1px solid #a94442')
        .css('background-color', '#f2dede')
        .after("<span class='form-validation' style='color:red'>Debe escribir una contraseña</span>");
    }
    if(admin_data["password_2"].length == 0){
        validation = false;
        $("#password_2")
        .css('border', '1px solid #a94442')
        .css('background-color', '#f2dede')
        .after("<span class='form-validation' style='color:red'>Debe escribir una contraseña</span>");
    }

    if(validation){

        var admin_data_update = {}
        admin_data_update.password = admin_data["password"];
        admin_data_update.id = $("#admin_id").val();

        $.ajax({
            type : "PUT",
            url : "/api/v1/admin_update/"+$("#admin_id").val()+"/",
            contentType: "application/json",
            data : JSON.stringify(admin_data_update),
            beforeSend: function(jqXHR, settings) {
                // Pull the token out of the DOM.
                jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
            }
        })
        .done(function(admin){
            $(".form-validation").remove();
            $("#success_api").show();
            
            window.setTimeout(function(){
                window.location.href = document.referrer;
            }, 1000);
        })
        .fail(function(e){
            //$("#success_api").hide();
            console.log(e)
            $(".form-validation").remove();
            $("form :input")
                .css('border', '')
                .css('background-color', '');
            $("#protocol_btn").text("Agregar nuevo protocolo");
            $("#alert_ok").hide();
            $("#alert_error").show();
            errors = JSON.parse(e["responseText"]);
            $.each(errors["user_update"], function(field, error){
                $("#id_"+field)
                    .css('border', '1px solid #a94442')
                    .css('background-color', '#f2dede')
                    .after("<span class='form-validation' style='color:red'>"+error+" </span>");
            })
        })
    }
}