var email = "";

$(function(){
	// $("#id_groups").removeAttr('multiple')
	getAdmin();
	$("#div_id_is_superuser").hide();
	$("#div_id_email").hide();
	$("#id_rut").attr('disabled', true);
	$("#btn_update").click(function(){
		updateAdministrator();
	})
	$(".input-custom").keyup(function () { 
      this.value = this.value.replace(/[^0-9]/g,'');
    });
	$('.admin-link').addClass('active-link');
})

function getAdmin(){
	$.ajax({
		type: "GET",
		url: "/api/v1/group/",
		async: false
	})
	.done(function(group){
		option_group = "";
		$.each(group["objects"], function(field, value){
			option_group += "<option value='" + value["id"] + "'>" + value["name"] + "</option>";
		})
		$("#id_group").html(option_group);
	})
	$.ajax({
		type : "GET",
		url : "/api/v1/user/"+$("#admin_id").val()+"/",
		async: false,
	})
	.done(function(admin){
		$.each(admin, function(field, value){
			if (field == 'contact_phone') {
				value = value.replace('+569', '');
			}
			if (field == 'group') {
				$("#id_"+field).val(value["id"]);
			} else {
				$("#id_"+field).val(value);
			}
		})
	})
	.fail(function(e){
		console.log(e)
	})
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function updateAdministrator(){
	var validation = true;
	var admin_data = $("form").serializeObject();

	admin_data["contact_phone"] = "+569" + admin_data["contact_phone"];

	$(".form-validation").remove();

	$.each(admin_data, function(field, value){
		if (validateEmail(value) == false && field == "email"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Error al validar email, por favor corríjalo y vuelva a intentar</span>");
		}
		if (value.length < 12 && field == "contact_phone"){
			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Error al validar teléfono, por favor corríjalo y vuelva a intentar</span>");
		}
		if (!(/^\S{3,}$/.test(value)) && field == "first_name"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Por favor ingrese su nombre sin espacios y vuelva a intentar</span>");
		}
		if (!(/^\S{3,}$/.test(value)) && field == "last_name"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Por favor ingrese su apellido sin espacios y vuelva a intentar</span>");
		}
	})

	if(validation){

		var admin_data_update = {}
		admin_data_update.first_name = admin_data["first_name"];
		admin_data_update.last_name = admin_data["last_name"];
		admin_data_update.contact_phone = admin_data["contact_phone"];
		admin_data_update.group = "/api/v1/group/"+admin_data["group"]+"/";
		admin_data_update.is_superuser = "on";

		if (email != admin_data["email"]) {
			admin_data_update.email = admin_data["email"];
		}

		console.log(JSON.stringify(admin_data_update));

		$.ajax({
			type : "PUT",
			url : "/api/v1/user_update/"+$("#admin_id").val()+"/",
			contentType: "application/json",
			data : JSON.stringify(admin_data_update),
			beforeSend: function(jqXHR, settings) {
	            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
	        }
		})
		.done(function(admin){
			$(".form-validation").remove();
			$("#success_api").show();
			
			window.setTimeout(function(){
		        window.location.href = document.referrer;
		    }, 1000);
		})
		.fail(function(e){
			//$("#success_api").hide();
			console.log(e)
			$(".form-validation").remove();
			$("form :input")
				.css('border', '')
				.css('background-color', '');
			$("#protocol_btn").text("Agregar nuevo protocolo");
			$("#alert_ok").hide();
			$("#alert_error").show();
			errors = JSON.parse(e["responseText"]);
			$.each(errors["user_update"], function(field, error){
				$("#id_"+field)
					.css('border', '1px solid #a94442')
					.css('background-color', '#f2dede')
					.after("<span class='form-validation' style='color:red'>"+error+" </span>");
			})
		})
	}
}