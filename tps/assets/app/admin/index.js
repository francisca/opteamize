$(function(){
	getAdmin("/api/v1/user/?is_superuser=1");
	$(".page").click(function(){
		getAdmin($(this).attr("data-url"));
	})
	$('.admin-link').addClass('active-link');
})

function getAdmin(resource_uri){
	$.ajax({
		type : "GET",
		url : resource_uri
	})
	.done(function(users){
		
		var rows = "";
		$.each(users["objects"], function(i, user){
			if(user["email"] == "") {
				email = "-";
			} else {
				email = user["email"]
			}

			if(user["group"] == null) {
				group = "-";
			} else {
				group = user["group"]["name"];
			}

			var btn_update = "<a href='/user/administrator/"+user["id"]+"/update/' class='btn btn-lg btn-icon btn-info'><i class='fa fa-edit'></i></a>";
			var btn_password = "<a href='/user/administrator/password/"+user["id"]+"/' class='btn btn-lg btn-icon btn-warning'><i class='fa fa-key'></a>"

			var row = "<tr>"+
				"<td>"+user["rut"]+"</td>"+
				"<td>"+user["last_name"] + ", " + user["first_name"]+"</td>"+
				"<td>"+group+"</td>"+
				"<td>"+user["contact_phone"]+"</td>"+
				"<td>"+email+"</td>"+
				"<td>"+btn_update+"&nbsp;&nbsp;"+btn_password+"</td>"+
				"</tr>";
			rows += row;
		});

		$("#admin_table tbody").html(rows);
		set_pagination_results(users);
	})
	.fail(function(e){
		console.log(e);
	})
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getAdmin(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#admin_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getAdmin(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#admin_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}