$(function(){
	//getProfiles();
	// $("#id_groups").removeAttr('multiple')
	$("#div_id_is_superuser").hide();
	$("#id_rut").keyup(function () { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});
	$('#id_contact_phone').keydown(function(e){onlyNumbers(e)});
	$("#id_contact_phone").keyup(function () { 
	    validatePhone($(this));
	});
	$("#id_password").attr('type', 'password');
	$(".btn_add").click(function(e){
		rut = $("#id_rut").val().split("");
		rut_length = rut.length;
		if (rut_length<8) {
			$(".form-validation").remove();
			$("form :input")
				.css('border', '')
				.css('background-color', '');
			$("#protocol_btn").text("Agregar Nuevo Administrador");
			$("#alert_ok").hide();
			$("#alert_error").show();
			$("#id_rut")
				.css('border', '1px solid #a94442')
				.css('background-color', '#f2dede')
				.after("<span class='form-validation' style='color:red'>Debe ingresar un RUT válido.</span>");
		} else {
			addAdministrator();
		}
	})
	$('.admin-link').addClass('active-link');

	document.getElementById("div_id_contact_phone").innerHTML = "<label for='last_name'>Teléfono</label>"+
	            "<div class='controls'>"+
	            	"<input type='text' class='textinput textInput input-custom col-xs-3 col-sm-1' value='+569' disabled='disabled'>"+
	            	"<input type='text' name='contact_phone' class='textinput textInput input-custom col-xs-9 col-sm-11' maxlength='8' id='id_contact_phone'>"+
	        	"</div>";

	$(".input-custom").keyup(function () { 
      this.value = this.value.replace(/[^0-9]/g,'');
    });
})

// function getProfiles(){
// 	$.ajax({
// 		type : "GET",
// 		url : "/api/v1/group/"
// 	})
// 	.done(function(groups){
// 		//console.log(groups)
// 		$.each(groups["objects"], function(i, group){
// 			$("#id_groups").append("<option value='"+group["id"]+"'>"+group["name"]+"</option>");
// 		})
// 	})
// 	.fail(function(e){
// 		console.log(e)
// 	})
// }

function addAdministrator(){
	var admin_data = $("form").serializeObject();
	
	// if (admin_data["groups"] == ""){
	// 	admin_data.groups = null;	
	// }
	// else{
	// 	//admin_data["groups"] = "/api/v1/group/"+admin_data["groups"]+"/";	
	// 	admin_data.groups = ["/api/v1/group/"+admin_data["groups"]+"/"];	
	// }

	if(rut[rut_length-9]){
		first_digit = rut[rut_length-9];
	} else {
		first_digit = "";
	}

	if(rut[rut_length-1] == "0") {
		rut_dv = "k";
	} else {
		rut_dv = rut[rut_length-1];
	}

	rut_valid = first_digit + rut[rut_length-8] + "." + rut[rut_length-7] + rut[rut_length-6] + rut[rut_length-5] + "." + rut[rut_length-4] + rut[rut_length-3] + rut[rut_length-2] + "-" + rut_dv;
	admin_data["is_superuser"] = "on";
	admin_data["rut"] = rut_valid;
	admin_data["contact_phone"] = "+569" + admin_data["contact_phone"];
	admin_data["group"] = "/api/v1/group/"+admin_data["group"]+"/";

	console.log(JSON.stringify(admin_data));
	
	$.ajax({
		type : "POST",
		url : "/api/v1/user/",
		contentType: "application/json",
		data : JSON.stringify(admin_data),
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
        }
	})
	.done(function(admin){
		$(".form-validation").remove();
		$("#success_api").show();

		window.setTimeout(function(){
	        window.location.href = document.referrer;
	    }, 1000);
	})
	.fail(function(e){
		//$("#success_api").hide();
		console.log(e)
		$(".form-validation").remove();
		$("form :input")
			.css('border', '')
			.css('background-color', '');
		$("#protocol_btn").text("Agregar Nuevo Administrador");
		$("#alert_ok").hide();
		$("#alert_error").show();
		errors = JSON.parse(e["responseText"]);
		$.each(errors["user"], function(field, error){
			$("#id_"+field)
				.css('border', '1px solid #a94442')
				.css('background-color', '#f2dede')
				.after("<span class='form-validation' style='color:red'>"+error+" </span>");
		})
	})
}