$(function(){
	getUser("/api/v1/user/?is_superuser=0");
	$(".page").click(function(){
		getUser($(this).attr("data-url"));
	})
	$('.user-link').addClass('active-link');

	$("#btn-search").on('click', searchUser);

	$("#btn-list").click(function(){
		$(".empty_query").hide('fast');
		$("#rut").val('');
		getUser("/api/v1/user/?is_superuser=0");
		$("html, body").delay(100).animate({scrollTop: $('#user_table').offset().top }, 800);
	});

	$("#rut").keyup(function () {
	  if($("#rut").val().length > 0){
	  	if(!$("#rut").val()[0].match(/[a-z]/i)){
	  	  $("#rut").attr('maxlength','12');
		  this.value = this.value.replace(/[^0-9\.\-\k]/g,'');
	      Rut(this.value);	
		}else{
	  	  $("#rut").attr('maxlength','30');
	  	  if(!$("#rut").val()[$("#rut").val().length-1].match(/[a-z ]/i)){
	  	  	document.getElementById("rut").value = $("#rut").val().substring(0, $("#rut").val().length - 1);
	  	  }
	    }
	  }
    });

	$('#open-filter').click(function(e){
		e.preventDefault();
		if($(this).hasClass('btn-filter-active')) {
			$('#box-filter-travel').hide('fast');
			$(this).removeClass('btn-filter-active');
		} else {
			$('#box-filter-travel').show('fast');
			$(this).addClass('btn-filter-active');
		}
	});

})

String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function searchUser(){
    var query = false;
    var filter = "";
    resource_uri = "/api/v1/user/?is_superuser=0";

    var rut = $('#rut').val().replace(/  +/g, ' ');
    console.log(rut);

    if (rut != "") {
        query = true;
    }

    if (query == true) {
        if(!rut.match(/[a-z]/i)){
	  		  filter += "&rut="+rut;
	  	}else{
	  		if(rut.split(" ")[1]){
	  		  filter += "&first_name__iexact="+rut.split(" ")[0].replaceAll(/ /, '')+"&last_name__iexact="+rut.split(" ")[1].replaceAll(/ /, '');
	  		}else{
	  		  filter += "&query="+rut.replaceAll(/ /, '');
	  		}
	  	}
    }
    console.log(filter);
    if (!query) {
        $(".empty_query").show('fast');
    } else {
        $('.empty_query').hide('fast');
        getUser(resource_uri+filter);
        $("html, body").delay(100).animate({scrollTop: $('#user_table').offset().top }, 800);
    }

}

function getUser(resource_uri){
	console.log(resource_uri)
	$.ajax({
		type : "GET",
		url : resource_uri
	})
	.done(function(users){
		console.log(users)
		var rows = "";
		$.each(users["objects"], function(i, user){
			if(user["email"] == "") {
				email = "-";
			} else {
				email = user["email"]
			}
			var row = "<tr>"+
				"<td>"+user["rut"]+"</td>"+
				"<td>"+user["last_name"] + ", " + user["first_name"]+"</td>"+
				"<td>"+user["contact_phone"]+"</td>"+
				"<td>"+email+"</td>"+
				"<td><a href='/user/"+user["id"]+"/' class='btn btn-lg btn-icon btn-info'><i class='fa fa-edit'></i></a></td>"+
				"</tr>";
			rows += row;
		});

		$("#user_table tbody").html(rows);
		set_pagination_results(users);
	})
	.fail(function(e){
		console.log(e);
	})
}

function set_pagination_results(meta_objet){
	var total_pages = Math.ceil(meta_objet["meta"]["total_count"] / 20);
	var resource_uri = "";
	if (total_pages == 0) {
		$('#page-selection').hide();
	}else{
		$('#page-selection').show();
		$('#page-selection').bootpag({
			total: total_pages,
			maxVisible: 20,
			leaps: true,
			firstLastUse: true,
			first: '←',
			last: '→'
		});
		$('ul.pagination.bootpag li').click(function() {
		    var num = $(this).attr('data-lp');
		    offset = parseInt(num - 1) * 20;

			if (meta_objet["meta"]["next"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["next"]);	
				getUser(resource_uri);
				$("html, body").delay(50).animate({scrollTop: $('#user_table').offset().top }, 800);
			}

			if(meta_objet["meta"]["previous"]) {
				resource_uri = replaceQueryParam("offset",offset,meta_objet["meta"]["previous"]);
				getUser(resource_uri);	
				$("html, body").delay(50).animate({scrollTop: $('#user_table').offset().top }, 800);
			}
		});
	}
}

function replaceQueryParam(param, newval, search) {
    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
    var query = search.replace(regex, "$1").replace(/&$/, '');
    return (query.length > 2 ? query + "&" : "?") + (newval ? param + "=" + newval : '');
}