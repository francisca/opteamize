$(function(){
	getUser();

	$("#btn_update").click(function(){
		updateUser();
	})

	$(".datepicker").datepicker({
		language : "es",
		format: 'yyyy-mm-dd'
	});

	$(".input-custom").keyup(function () { 
      this.value = this.value.replace(/[^0-9]/g,'');
    });
    $('.user-link').addClass('active-link');
})

function getUser(){
	$.ajax({
		type : "GET",
		url : "/api/v1/user/"+$("#user_id").val()+"/",
		async: false,
	})
	.done(function(admin){
		$.each(admin, function(field, value){
			if (field == 'emergency_phone') {
				value = value.replace('+56', '');
			}
			if (field == 'contact_phone') {
				value = value.replace('+569', '');
			}
			$("#id_"+field).val(value);
		})
	})
	.fail(function(e){
		console.log(e)
	})
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function updateUser(){
	var validation = true;
	var admin_data = $("form").serializeObject();

	admin_data["contact_phone"] = "+569" + admin_data["contact_phone"];

	$(".form-validation").remove();

	$.each(admin_data, function(field, value){
		if (validateEmail(value) == false && field == "email"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Error al validar email, por favor corríjalo y vuelva a intentar</span>");
		}
		if (value.length < 12 && field == "contact_phone"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Error al validar teléfono, por favor corríjalo y vuelva a intentar</span>");
		}
		if (!(/^\S{3,}$/.test(value)) && field == "first_name"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Por favor ingrese su nombre sin espacios y vuelva a intentar</span>");
		}
		if (!(/^\S{3,}$/.test(value)) && field == "last_name"){

			validation = false;
			$("#id_"+field)
			.css('border', '1px solid #a94442')
			.css('background-color', '#f2dede')
			.after("<span class='form-validation' style='color:red'>Por favor ingrese su apellido sin espacios y vuelva a intentar</span>");
		}
	})

	if(validation){

		$.ajax({
			type : "PUT",
			url : "/api/v1/user_update/"+$("#user_id").val()+"/",
			contentType: "application/json",
			data : JSON.stringify(admin_data),
			beforeSend: function(jqXHR, settings) {
	            // Pull the token out of the DOM.
	            jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
	        }
		})
		.done(function(admin){
			$(".form-validation").remove();
			$("#success_api").show();
			
			window.setTimeout(function(){
		        window.location.href = document.referrer;
		    }, 1000);
		})
		.fail(function(e){
			//$("#success_api").hide();
			console.log(e)
			$(".form-validation").remove();
			$("form :input")
				.css('border', '')
				.css('background-color', '');
			$("#alert_ok").hide();
			$("#alert_error").show();
			errors = JSON.parse(e["responseText"]);
			$.each(errors["user_update"], function(field, error){
				$("#id_"+field)
					.css('border', '1px solid #a94442')
					.css('background-color', '#f2dede')
					.after("<span class='form-validation' style='color:red'>"+error+" </span>");
			})
		})

	}
}