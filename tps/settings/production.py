from tps.settings.base import *

DEBUG = False
ALLOWED_HOSTS = ['tpsgo.tpsv.cl']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'tps_go',
        'USER': 'postgres',
        'PASSWORD': 'tpsgo2017',
        'HOST': '',
        'PORT': '',
    }
}