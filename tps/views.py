# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import FormView, TemplateView
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib import messages

from users.forms import AuthForm
from users.models import User


class AuthView(FormView):
    template_name = 'login.html'
    form_class = AuthForm
    success_url = reverse_lazy('postulant_list')
    form_invalid_message = u'Error al ingresar datos de usuario.'

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['username'],
                            password=form.cleaned_data['password'])

        if user:
            user_row  = User.objects.get(id=user.id)
            if user_row.is_superuser:
                login(self.request, user)
                return HttpResponseRedirect(self.get_success_url())
            else:
                messages.error(self.request, "El usuario ingresado no tiene los permisos necesarios para iniciar sesión como administrador. Intente nuevamente.")
                return HttpResponseRedirect(reverse_lazy('login'))
        else:
            messages.error(self.request, "El correo o password son incorrectos. Intente nuevamente.")
            return HttpResponseRedirect(reverse_lazy('login'))

    def form_invalid(self, form):
        messages.error(self.request, "Error al iniciar sesión. Intente nuevamente.")
        return HttpResponseRedirect(reverse_lazy('login'))


    # def get(self, request, *args, **kwargs):
    #     if request.user.is_anonymous():
    #         return self.render_to_response(self.get_context_data())
    #     else:
    #         return HttpResponseRedirect(reverse_lazy('stats_view'))